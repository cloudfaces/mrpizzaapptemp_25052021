//
//  main.m
//  mobile-pocket
//
//  Created by Simon Moser on 30.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @try
    {
        @autoreleasepool {
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        }
    }
    @catch(NSException* err)
    {
        NSLog(@"main thread error, name: %@", err.name);
        NSLog(@"main thread error, reason: %@", err.reason);
        NSLog(@"main thread error, description: %@", err.description);
        NSLog(@"main thread error, userInfo: %@", err.userInfo);
    }
    @finally
    {
        
    }
}
