//
//  BundleSettings.m
//  app
//
//  Created by Simon Moser on 05.04.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BundleSettings.h"


@implementation BundleSettings

- (instancetype) init
{
    self = [super init];
    
    if (self) {
        _overrideRemoteApp = NO;
    }
    
    return self;
}

@end
