//
//  Notification.h
//  app
//
//  Created by Bernhard Kunnert on 08.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#ifndef app_Notification_h
#define app_Notification_h

#define NOTIFICATION_LOCATION_UPDATE @"CurrentLocationUpdated"
#define NOTIFICATION_LOCATION_UPDATEa @"CurrentLocationUpdateda"

#define NOTIFICATION_GEO_FENCE_ENTRY @"GeoFenceEntry"
#define NOTIFICATION_GEO_FENCE_EXIT @"GeoFenceExit"
#define NOTIFICATION_MESSAGE_OPENED @"PushMessageOpened"
#define NOTIFICATION_NATIVE_BRIDGE_LOADED @"CFLoaded"

#endif
