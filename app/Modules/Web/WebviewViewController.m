//
//  WebviewViewController.m
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "WebviewViewController.h"
#import "CFHtmlPage.h"
#import "NativeBridge.h"
#import "CFRuntime.h"
#import "Storage.h"
#import "DataPool.h"
#import "ScannerViewController.h"
#import "Navigation.h"
#import "CFHttpRequest.h"
#import "FacebookHelper.h"
#import "AppDownloadViewController.h"
#import "BundleSettings+Additions.h"
#import "LocationRetriever.h"
#import "BackgroundLocationRetriever.h"
#import "LocationCoder.h"
#import "LocationFencer.h"
#import <EventKit/EventKit.h>
#import "CFIdentity.h"
#import "CFShare.h"
#import "ExternalContentViewController.h"
#import "CFPage+JSON.h"
#import "Contacts.h"
#import "Badges.h"
#import "CalendarEvents.h"
#import "Dialog.h"
#import "PayPalViewController.h"
#import "GoogleAnalyticsViewController.h"
#import "Reachability.h"
#import "SmartLink.h"
#import "CFHttpResponseHelper.h"
#import <AudioToolbox/AudioToolbox.h>
@import WatchConnectivity;
#import "AppDownloadViewHelper.h"
//#import <BeaconService/BIBeaconService.h>
#import "AppDelegate.h"
#import "UIImage+fixOrientation.h"
#import "MRProgress.h"
#import "SlidingTabsHelper.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface WebviewViewController () <NativeBridgeDelegate, ScannerViewControllerDelegate, CFHttpRequestDelegate, AppDownloadViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,
//    PayPalViewControllerDelegate, PayPalPaymentDelegate,
    ReachabilityDelegate, SmartLinkDelegate, CFHttpResponseHelperDelegate, AppDownloadViewHelperDelegate, NSURLConnectionDelegate,FBSDKLoginButtonDelegate, LocationRetrieverDelegate>
{
    NSMutableData *_responseData;
    DataPool* _dataPool;
    Storage* _storage;
    NSInteger _scannerCallbackId;
    NSInteger _imagePickerCallbackId;
    BOOL _imagePickerDataResult;
    NSMutableArray* notifications;
    BOOL _initialized;
    NSInteger _payPalCallbackId;
    NSString* startTrackingUrl;
    
}

@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (strong, nonatomic) IBOutlet NativeBridge *nativeBridge;
@property (strong, nonatomic) CFHeaderButton* rightButton;
@property (strong, nonatomic) CFHeaderButton* leftButton;
@property (strong, nonatomic) PayPalViewController* payPalViewController;
@property (strong, nonatomic) CFContentView* contentView;

@property (strong, nonatomic) LocationRetriever* currentLocationRetriever;
@property (nonatomic)  BOOL isActive;
@property (nonatomic)  BOOL nativeBridgeLoaded;
@property Contacts *context;
@property Badges *badgesContext;
@property CFHttpResponse *responseContext;
@property CalendarEvents *calendarEventsContext;
@property Dialog *dialogContext;
@property (strong, nonatomic) NSMutableArray  *contacts;
@property (strong, nonatomic) NSString *calendarsForEvents;
@property (strong, nonatomic) NSString *visibiality;
@property (strong, nonatomic) NSString *callBackIDString;
//@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

@property (strong, nonatomic) NSString *responseBody;

@property (strong, nonatomic) IBOutlet Reachability *reachability;
@property (strong, nonatomic) IBOutlet SmartLink *smartLink;
@property (strong, nonatomic) IBOutlet CFHttpResponseHelper *cFHttpResponseHelper;

//@property (nonatomic, weak) id<FBSDKAppInviteDialogDelegate> delegate;
NSString *cleanString(NSString *string, NSString *search);
NSString *cleanFile(NSString *string);

@end
int i = 0;
int j = 0;
@implementation WebviewViewController



NSString * cleanString(NSString *string, NSString *search){
    return [string stringByReplacingOccurrencesOfString:search withString:@""];
}
NSString * cleanFile(NSString *string){
    return cleanString(string, @"file://");
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization:
        _storage = [Storage instance];
        _dataPool = [DataPool instance];
        self.currentLocationRetriever = [[LocationRetriever alloc] init];
        self->_initialized = NO;
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    notifications = [[NSMutableArray alloc] init];
    _reachability.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:@"BeaconServiceRegionEnter"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:@"BeaconServiceRegionUpdate"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:@"BeaconServiceRegionExit"
                                               object:nil];
    
    [self updateAppearance];
}

- (void) updateAppearance
{
    [super updateAppearance];
    CFHtmlPage* htmlPage;

    if (!self.typedPage) {
        htmlPage = [[CFHtmlPage alloc] initWithPage:self.navigationService.page];
        self.typedPage = htmlPage;
    } else {
        htmlPage = (CFHtmlPage*) self.typedPage;
    }

    self.webView.backgroundColor = htmlPage.backgroundColor;
    //self.webView.delegate = self;
    self.navigationController.navigationBarHidden = htmlPage.fullScreen;
    [self.webView setBackgroundColor: htmlPage.backgroundColor];
    [self.webView setOpaque:NO];

    NSURL *url;

    if ([BundleSettings isRemoteApp]) {
        url = [NSURL URLWithString:htmlPage.source relativeToURL:[NSURL URLWithString:[CFRuntime instance].baseUrl]];
    } else {
        url = [NSURL fileURLWithPath:[CFRuntime pathForAppFile:htmlPage.source ofType:nil inDirectory:nil]];
    }

    if (!self->_initialized) {
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [self.webView setBackgroundColor:htmlPage.backgroundColor];
        [self.webView setOpaque:NO];
        
//        if (SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"13.0")) {
//            NSLog(@"Pavel - ios version >= 13.0, %s", __VERSION__);
//
//            [self.webView loadRequest:requestObj];
//        } else {
//            NSLog(@"Pavel - ios version < 13.0, %s", __VERSION__);

            if ([url.absoluteString hasPrefix:@"file://"]) {
//                NSURL *b = [NSURL URLWithString:NSBundle.mainBundle.resourcePath relativeToURL:url];
//file:///Users/pavelgeorgiev/Library/Developer/CoreSimulator/Devices/BDC611E4-2908-4CCA-957C-0BDD85D5C224/data/Containers/Bundle/Application/0F4E0084-DA6D-420D-82CA-B4414245889D/CloudFaces.app
                NSURL *burl = NSBundle.mainBundle.resourceURL;
//file:///Users/pavelgeorgiev/Library/Developer/CoreSimulator/Devices/BDC611E4-2908-4CCA-957C-0BDD85D5C224/data/Containers/Bundle/Application/D4C3932A-56EF-4740-AB1B-D5FCB0F12013/NNF.app/
                
                NSRange brpf = [url.absoluteString rangeOfString:@"/www/" options:NSBackwardsSearch];
                NSRange brpr = [url.absoluteString rangeOfString:@"/" options:NSBackwardsSearch];

//                NSLog(@"Pavel - bundle: %@", b.absoluteString);
//                NSLog(@"Pavel - bundle url: %@", burl.absoluteString);
//                NSLog(@"Pavel - url: %@", url.absoluteString);
//                NSLog(@"Pavel - url s: %lu", (unsigned long)brpf.location);
//                NSLog(@"Pavel - url e: %lu", (unsigned long)brpr.location);

                NSString *brp = [url.absoluteString substringWithRange:NSMakeRange(brpf.location, brpr.location-brpf.location)];
                brp = [brp stringByAppendingString:@"/"];

//                NSLog(@"Pavel - url pre-base: %@", brp);
                brp = [burl.absoluteString stringByAppendingString:brp];
//                NSLog(@"Pavel - url base: %@", brp);

                NSURL *bpurl = [NSURL URLWithString:brp];
                NSString *sHtml = [NSString stringWithContentsOfFile:url.path encoding:NSUTF8StringEncoding error:nil];

                [self.webView loadHTMLString:sHtml baseURL:bpurl];
            } else {
                [self.webView loadRequest:requestObj];
            }
//        }
        
        self->_initialized = YES;
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

//    if([self isViewLoaded] && self.view.window == nil)
//    {
//        self.view = nil;
//    }
}

#pragma mark NativeBridgeDelegate

- (void) nativeBridge:(NativeBridge*) bridge handleCall:(NSString*) functionName withArguments:(NSArray*) arguments callbackId:(NSInteger) callbackId forWebView:(WKWebView*) webView
{
//    NSLog(@"Pavel - nativeBridge handleCall withArguments callbackId forWebView - functionName: %@", functionName);
    
    if ([functionName isEqualToString:@"CFNavigation.navigate"]) {
        CFPage* page = [[CFRuntime instance] getPageWithId:arguments[0]];
        [self.navigationService navigateToPage:page withContext:arguments[1]];
    } else if ([functionName isEqualToString:@"CFMenuNavigation.navigate"]) {
        CFPage* page = [[CFRuntime instance] getPageWithId:arguments[0]];
        [self tryMenuNavigation:page withContext:arguments[1]];
    } else if ([functionName isEqualToString:@"CFNavigation.navigateBack"]) {
        [self.navigationService navigateBackWithResult:arguments[0]];
    } else if ([functionName isEqualToString:@"CFNavigation.navigateAndAddToBackStack"]) {
        CFPage* page = [[CFRuntime instance] getPageWithId:arguments[0]];
        [self.navigationService navigateToPage:page withContext:arguments[1]];
    } else if ([functionName isEqualToString:@"CFNavigation.navigateBackFromStack"]) {
        [self.navigationService navigateBackWithResult:arguments[1] onStackView:arguments[0]];
    } else if ([functionName isEqualToString:@"CFNavigation.navigateToRoot"]) {
        if ([arguments[0] isEqualToString:@""]) {
            [self.navigationService navigateBackWithResultToRoot:arguments[1] onStackView:arguments[0]];
        } else {
            if ([SlidingTabsHelper sharedInstance].slidingTabs) {
                [[SlidingTabsHelper sharedInstance] setSlidingTabsEnable:@"YES"];
                [SlidingTabsHelper sharedInstance].navContext = arguments[1];
                
                [SlidingTabsHelper sharedInstance].pageId = arguments[0];
            }
            CFPage* page = [[CFRuntime instance] getPageWithId:arguments[0]];
            [self tryMenuNavigationFromStack:page withContext:arguments[1]];
        }
    } else if ([functionName isEqualToString:@"CFNavigation.getNavigationContext"]) {
        
        if ([SlidingTabsHelper sharedInstance].navContext == (id)[NSNull null] || [SlidingTabsHelper sharedInstance].navContext.length == 0 ) {
            [bridge returnResult:callbackId synchronously:YES args:self.navigationService.navigationContext, nil];
            
        }else{
            [bridge returnResult:callbackId synchronously:YES args:[SlidingTabsHelper sharedInstance].navContext, nil];
            
        }
        
    } else if ([functionName isEqualToString:@"CFPersistentStorage.deleteValue"]) {
        [_storage removeValue];
    } else if ([functionName isEqualToString:@"CFPersistentStorage.valueExists"]) {
        BOOL valueExists = [_storage valueExistsForKey:arguments[0]];
//        NSLog(@"Pavel - CFPersistentStorage.valueExists - for key %@ with value %@", arguments[0], valueExists?@"YES":@"NO");
        [bridge returnResult:callbackId synchronously:YES args:[NSNumber numberWithBool:valueExists], nil];
    } else if ([functionName isEqualToString:@"CFPersistentStorage.readValue"]) {
        NSString* value = [_storage readValueForKey:arguments[0]];
//        NSLog(@"Pavel - CFPersistentStorage.readValue - for key %@ with value %@, callbackId: %ld", arguments[0], value, (long)callbackId);
        [bridge returnResult:callbackId synchronously:YES args:value, nil];
        
    } else if ([functionName isEqualToString:@"CFPersistentStorage.writeValue"]) {
        [_storage writeValue:arguments[1] forKey:arguments[0]];
        [_storage commitChanges];
        
        if ([arguments[0] isEqual:@"app_hash_url"]) {
            NSString *valueToSave = arguments[1];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *appHashUrl = [prefs stringForKey:@"app_hash_url"];
            
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"app_hash_url"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [_storage writeValue:arguments[1] forKey:arguments[0]];
            [_storage commitChanges];
            
            if (appHashUrl == (id)[NSNull null] || appHashUrl.length == 0 ) {
                
                //reload app - first install
                NSDictionary *response = [self getDataFrom:valueToSave];
                NSString *status = response[@"Status"];
                
                if ([status isEqual:@"Success"]) {
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    UIWindow* window = [UIApplication sharedApplication].keyWindow;
                    if (!window) {
                        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
                    }
                    
//                    [MRProgressOverlayView showOverlayAddedTo:[[window subviews] objectAtIndex:0].superview animated:YES];
//                    [appDelegate codePush];
                }
            }
        }
        
        if ([arguments[0] isEqual:@"app_id"]) {
            NSString *valueToSave = [NSString stringWithFormat:@"%@", arguments[1]];
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"app_id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
//        NSLog(@"Pavel - CFPersistentStorage.writeValue - for key %@ with value %@", arguments[0], [NSString stringWithFormat:@"%@", arguments[1]]);
        
    } else if ([functionName isEqualToString:@"CFVariableStorage.valueExists"]) {
        BOOL valueExists = [_dataPool valueExistsForKey:arguments[0]];
//        NSLog(@"Pavel - CFVariableStorage.valueExists - for key %@ with value %@", arguments[0], valueExists?@"YES":@"NO");
        [bridge returnResult:callbackId synchronously:YES args:[NSNumber numberWithBool:valueExists], nil];
    } else if ([functionName isEqualToString:@"CFVariableStorage.readValue"]) {
        NSString* value = [_dataPool readValueForKey:arguments[0]];
//        NSLog(@"Pavel - CFVariableStorage.readValue - for key %@ with value %@", arguments[0], value);
        [bridge returnResult:callbackId synchronously:YES args:value, nil];
    } else if ([functionName isEqualToString:@"CFVariableStorage.writeValue"]) {
//        NSLog(@"Pavel - CFVariableStorage.writeValue - for key %@ with value %@", arguments[0], [NSString stringWithFormat:@"%@", arguments[1]]);
        [_dataPool writeValue:arguments[1] forKey:arguments[0]];
    } else if ([functionName isEqualToString:@"CFVariableStorage.deleteValue"]) {
//        NSLog(@"Pavel - CFVariableStorage.deleteValue");
        [_dataPool removeValue];
        // [bridge returnResult:callbackId synchronously:YES args:value, nil];
    } else if ([functionName isEqualToString:@"CFScanner.scanBarCode"]) {
        _scannerCallbackId = callbackId;
        ScannerViewController* viewController = [[ScannerViewController alloc] init];
        BaseNavigationController* _scannerViewController = [[BaseNavigationController alloc] initWithRootViewController:viewController];
        viewController.delegate = self;
        [self presentViewController:_scannerViewController animated:YES completion:nil];
        
    } else if ([functionName isEqualToString:@"CFNetwork.sendHttpRequest"]) {
        CFHttpRequest* request = [[CFHttpRequest alloc] init];
        request.callbackId = callbackId;
        NSDictionary* params = arguments[0];
        
        request.url = params[@"Url"];
        request.method = params[@"Method"];
        request.body = params[@"Body"];
        request.headers = params[@"Headers"];
        request.delegate = self;
        
//        NSLog(@"Pavel - CFNetwork.sendHttpRequest - m: %@, u: %@, b: %@...", request.method, request.url, request.body);
        
        [request send];
    } else if ([functionName isEqualToString:@"CFNetwork.sendHttpRequestBinary"]) {
        CFHttpRequest* request = [[CFHttpRequest alloc] init];
        request.callbackId = callbackId;
        NSDictionary* params = arguments[0];
        
        request.url = params[@"Url"];
        request.method = params[@"Method"];
        request.filePath = [NSURL URLWithString:params[@"BinaryFile"]].path;
        request.headers = params[@"Headers"];
        request.delegate = self;
        
//        NSLog(@"Pavel - CFNetwork.sendHttpRequestBinary - m: %@, u: %@, b: %@...", request.method, request.url, request.body);
        
        [request sendFile];
        
    } else if ([functionName isEqualToString:@"CFNetwork.getConnectionType"]) {
        NSString* connectionType = [[CFRuntime instance] getConnectionType];
        [self.nativeBridge returnResult:callbackId synchronously:NO args:connectionType, nil];
        
    } else if ([functionName isEqualToString:@"CFFacebook.login"]) {
        NSInteger cbId = callbackId;
        NSString *permissions = arguments[0];
        
        [[FacebookHelper instance] login:^(NSDictionary *resultDictionary) {
            [bridge returnResult:cbId synchronously:NO args:resultDictionary, nil];
            
        } permissions:permissions];
        
    } else if ([functionName isEqualToString:@"CFFacebook.isLoggedIn"]) {
        [bridge returnResult:callbackId synchronously:YES args:[NSNumber numberWithBool:[FacebookHelper instance].accessToken != nil], nil];
        
    } else if ([functionName isEqualToString:@"CFFacebook.getToken"]) {
        if ([FacebookHelper instance].accessToken) {
            [bridge returnResult:callbackId synchronously:NO args:[FacebookHelper instance].accessToken, nil];
        } else {
            [bridge returnResult:callbackId synchronously:NO args:@"", nil];
        }
        
    } else if ([functionName isEqualToString:@"CFFacebook.logout"]) {
        [[FacebookHelper instance] logout];
        
    } else if ([functionName isEqualToString:@"CFFacebook.appInvites"]) {
//        NSLog(@"NativeBridge - facebook app invites is deprecated, no longer supported");
        
        //NSString *appLinkString = arguments[0];
        //NSString *previewImageString = arguments[1];
        //FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
        //content.appLinkURL = [NSURL URLWithString:appLinkString];
        //optionally set previewImageURL
        //content.appInvitePreviewImageURL = [NSURL URLWithString:previewImageString];
        
        //[FBSDKAppInviteDialog showWithContent:content delegate:self];
        
        
        
        // Present the dialog. Assumes self is a view controller
        // which implements the protocol `FBSDKAppInviteDialogDelegate`.
        //        [FBSDKAppInviteDialog showFromViewController:self withContent:content delegate:self];
        
        
        //        FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
        //        shareDialog.delegate=self;
        //        shareDialog.fromViewController = self;
        //        shareDialog.shareContent = content;
        //        [shareDialog show];
        
        
        //        FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
        //        content.appLinkURL = [NSURL URLWithString:@"https://fb.me/862131003890641"];
        //        //optionally set previewImageURL
        //     //   content.previewImageURL = [NSURL URLWithString:@""];
        //        [FBSDKAppInviteDialog showWithContent:content delegate:self];
        
        //        //    content.previewImageURL
        
        
        
        // Present the dialog. Assumes self is a view controller
        // which implements the protocol `FBSDKAppInviteDialogDelegate`.
        
        
        //[FBSDKAppInviteDialog showWithContent:content delegate:self];
        
        //    [[FacebookHelper instance] appInvates:appLinkString andPreviewUrl:previewImageString];
        
        
        
        
    } else if ([functionName isEqualToString:@"CFRuntime.loadApp"]) {
        [BundleSettings setOverrideRemoteApp:YES];
        NSURL* url = [NSURL URLWithString:arguments[0]];
        [self initRuntimeFromUrl:url];
        
    } else if ([functionName isEqualToString:@"CFRuntime.log"]) {
        [AppDownloadViewHelper sharedInstance].delegate = self;
        NSString *type = @"";
        NSString *time = @"";
        NSString *message = @"";
        NSString *color = @"";
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:type, @"type",time, @"time",message, @"message",color, @"color", nil];
        
        if (arguments.count > 0) {
            NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
            
            [DateFormatter setDateFormat:@"yyyyMMdd HH:mm:ss.SSS"];
            time = [DateFormatter stringFromDate:[NSDate date]];
            
            if (arguments[0] == [NSNull null]) {
                // invalid message
            } else {
                NSString *strValueMSG = [arguments[0] stringValue];
                message = [strValueMSG  stringByRemovingPercentEncoding];    //brijesh
                message = strValueMSG;
            }
            
            if  ([arguments[1] isEqual:nil] || [arguments[1] isEqual:[NSNull null]] || !(arguments[1])) {
                type = @"1";
                color = @"#000000";
            } else {
                type = [arguments[1] stringValue];
                if ([type isEqual:@"1"]) {
                    color = @"#000000";
                }
                if ([type isEqual:@"2"]) {
                    color = @"#FF0000";
                }
                if ([type isEqual:@"3"]) {
                    color = @"#FFAA00";
                }
                if ([type isEqual:@"4"]) {
                    color = @"#00AA00";
                }
                if ([type isEqual:@"5"]) {
                    color = @"#0000FF";
                }
            }
            
            [dict setObject:type forKey:@"type"];
            [dict setObject:time forKey:@"time"];
            [dict setObject:message forKey:@"message"];
            [dict setObject:color forKey:@"color"];
            
            if (([type isEqual:@"2"] && [[AppDownloadViewHelper sharedInstance].errorsSwitch isEqualToString:@"true"]) || ([type isEqual:@"3"] && [[AppDownloadViewHelper sharedInstance].warningsSwitch isEqualToString:@"true"]) || ([type isEqual:@"4"] && [[AppDownloadViewHelper sharedInstance].infoSwitch isEqualToString:@"true"]) || ([type isEqual:@"5"] && [[AppDownloadViewHelper sharedInstance].debugSwitch isEqualToString:@"true"]) || [type isEqual:@"1"]) {
                [[AppDownloadViewHelper sharedInstance].logArray addObject:dict];
            }
            
            NSArray *logArray = [AppDownloadViewHelper sharedInstance].logArray;
            
            NSArray *typeArray = [logArray valueForKey:@"type"];
            
            int force    = 0;
            int errors   = 0;
            int warnings = 0;
            int info     = 0;
            int debug    = 0;
            
            for(NSString *string in typeArray){
                force += ([string isEqualToString:@"1"]?1:0); //certain object is @"1"
                errors += ([string isEqualToString:@"2"]?1:0); //certain object is @"2"
                warnings += ([string isEqualToString:@"3"]?1:0); //certain object is @"3"
                info += ([string isEqualToString:@"4"]?1:0); //certain object is @"4"
                debug += ([string isEqualToString:@"5"]?1:0); //certain object is @"5"
            }
            
//            NSLog(@"number of  force1 %d", force);
//            NSLog(@"number of  errors %d", errors);
//            NSLog(@"number of  warnings %d", warnings);
//            NSLog(@"number of  info %d", info);
//            NSLog(@"number of  debug %d", debug);
            
            [[CFRuntime instance] setOverlayErrorLabel:[@(errors) stringValue]];
            [[CFRuntime instance] setOverlayWarnLabel:[@(warnings) stringValue]];
            [[CFRuntime instance] changeErrorLabel:[@(errors) stringValue]];
            [[CFRuntime instance] changeWarnLabel:[@(warnings) stringValue]];
            
            [AppDownloadViewHelper sharedInstance].forceCount  = force;
            [AppDownloadViewHelper sharedInstance].errorsCount = errors;
            [AppDownloadViewHelper sharedInstance].warnCount   = warnings;
            [AppDownloadViewHelper sharedInstance].infoCount   = info;
            [AppDownloadViewHelper sharedInstance].debugCount  = debug;
        }
        
        if (arguments.count == 2) {
            NSLog(@"Pavel - CFRuntime.log - argument: %@, argument: %@", arguments[0], arguments[1]);
        } else {
            NSLog(@"Pavel - CFRuntime.log - arguments %lu", (unsigned long)arguments.count);
        }
        
    } else if ([functionName isEqualToString:@"CFPushNotifications.getToken"]) {
        NSLog(@"Pavel - CFPushNotifications.getToken - token %@", [BundleSettings pushToken]);
        
        [bridge returnResult:callbackId synchronously:YES args:[BundleSettings pushToken], nil];
        
    } else if ([functionName isEqualToString:@"CFCalendarEvents.createEvent"]) {
        
        NSString* title = arguments[0];
        
        NSDateComponents* dateComponents = [[NSDateComponents alloc] init];
        dateComponents.year   = [arguments[1] integerValue];
        dateComponents.month  = [arguments[2] integerValue];
        dateComponents.day    = [arguments[3] integerValue];
        dateComponents.hour   = [arguments[4] integerValue];
        dateComponents.minute = [arguments[5] integerValue];
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDate *date = [gregorian dateFromComponents:dateComponents];
        
        NSTimeInterval duration = [arguments[6] integerValue] * 60;
        
        [self saveToCalendar:title date:date duration:duration resultBlock:^(BOOL result) {
            [bridge returnResult:callbackId synchronously:NO args:[NSNumber numberWithBool:result], nil];
        }];
        
    }else if ([functionName isEqualToString:@"CFCalendarEvents.getCalendars"]) {
        
        [[CalendarEvents sharedInstance] getAllCalendars:^(NSDictionary *resultDictionary) {
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[resultDictionary valueForKey:@"allCalendras"]  options:0 error:nil];
            
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
            [bridge returnResult:callbackId synchronously:NO args:jsonArray, nil];
            
        }];
        
    } else if ([functionName isEqualToString:@"CFCalendarEvents.insertEvent"]) {
        
        NSString* calendarIdsForEvent = arguments[0];
        
        NSString* startDateString = arguments[1];
        NSString* endDateString = arguments[2];
        NSTimeInterval beginTime = [startDateString doubleValue];
        NSTimeInterval endTime = [endDateString doubleValue];
        NSDate * startDate = [NSDate dateWithTimeIntervalSince1970:beginTime / 1000.0];
        NSDate * endDate = [NSDate dateWithTimeIntervalSince1970:endTime / 1000.0];
        
        NSString* title = arguments[3];
        NSString *notes = arguments[4];
        NSString *reminder;
        
        if ([arguments[5] isEqual:@""] ){
            
            reminder  = arguments[5];
        }
        else
        {
            long int  time = [arguments[5] integerValue] * 60;
            reminder = [NSString stringWithFormat:@"-%ld", time];
        }
        NSString *location = arguments[6];
        
        
        self.calendarEventsContext = [CalendarEvents sharedInstance];
        
        NSArray *eventsId = [self.calendarEventsContext saveToCalendar:calendarIdsForEvent title:title startDate:startDate endDate:endDate notes:notes  reminder:reminder location:location];
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:eventsId options:0 error:nil];
        NSArray *eventIds = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        
        [bridge returnResult:callbackId synchronously:NO args:eventIds, nil];
        
    }else if ([functionName isEqualToString:@"CFCalendarEvents.updateEvent"]) {
        
        NSString *eventId = arguments[0];
        
        NSString *startDateString = arguments[1];
        NSString *endDateString = arguments[2];
        NSTimeInterval beginTime = [startDateString doubleValue];
        NSTimeInterval endTime = [endDateString doubleValue];
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:beginTime / 1000.0];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endTime / 1000.0];
        
        NSString *title = arguments[3];
        NSString *notes = arguments[4];
        
        NSString *reminder;
        
        if ([arguments[5] isEqual:@""] ){
            
            reminder  = arguments[5];
        }
        else
        {
            long int  time = [arguments[5] integerValue] * 60;
            reminder = [NSString stringWithFormat:@"-%ld", time];
        }
        
        NSString *location = arguments[6];
        
        
        self.calendarEventsContext = [CalendarEvents sharedInstance];
        
        BOOL success = [self.calendarEventsContext updateEvent:eventId title:title startDate:startDate endDate:endDate notes:notes reminder:reminder location:location];
        
        [bridge returnResult:callbackId synchronously:NO args:@(success), nil];
        
    }else if ([functionName isEqualToString:@"CFCalendarEvents.deleteEvent"]) {
        
        NSString* eventId = arguments[0];
        BOOL success = [self.calendarEventsContext deleteEvent:eventId];
        [bridge returnResult:callbackId synchronously:NO args:@(success), nil];
        
    }else if ([functionName isEqualToString:@"CFDialog.show"]) {
        
        NSString *type    = arguments[0];
        NSString *title   = arguments[1];
        NSString *actions = arguments[2];
        NSString *data    = arguments[3];
        
        self.dialogContext = [Dialog sharedInstance];
        UIAlertController *alertController = [self.dialogContext openDialogType:type title:title actions:actions data:data];
        
        
        if ((![actions isEqualToString:@""]) || (actions != nil)) {
            
            
            NSError *jsonError;
            NSData *actionData   = [actions dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *action = [NSJSONSerialization JSONObjectWithData:actionData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&jsonError];
            
            
            NSDictionary *negative           = action[@"negative"];
            NSString *negativeButtonLabel    = negative[@"label"];
            NSString *negativeButtonFunction = negative[@"function"];
            
            NSDictionary *positive           = action[@"positive"];
            NSString *positiveButtonLabel    = positive[@"label"];
            NSString *positiveButtonFunction = positive[@"function"];
            
            NSDictionary *neutral            = action[@"neutral"];
            NSString *neutralButtonLabel     = neutral[@"label"];
            NSString *neutralButtonFunction  = neutral[@"function"];
            
            
            if ([negativeButtonLabel length] == 0) {
//                NSLog(@"negativeButtonLabel isEqual sEqual: 0");
            }else{
                if ((![negativeButtonLabel isEqualToString:@""]) || ([negativeButtonLabel length] != 0) || (negativeButtonLabel !=nil) ){
                    UIAlertAction *negativeButton = [UIAlertAction
                                                     actionWithTitle:negativeButtonLabel
                                                     style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                                                     {
                                                         
                                                         NSMutableDictionary * resultDatas = [self.dialogContext getResultData];
                                                         
                                                         [resultDatas setValue:negativeButtonFunction forKey:@"function"];
                                                         
                                                         NSError *error;
                                                         NSData *jsonData = [NSJSONSerialization dataWithJSONObject: resultDatas                         options:NSJSONWritingPrettyPrinted                                      error:&error];
                                                         NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
                                                         
                                                         [bridge returnResult:callbackId synchronously:NO args:jsonArray, nil];
                                                     }];
                    
                    [alertController addAction:negativeButton];
                }
            }
            
            if ([positiveButtonLabel length] == 0) {
//                NSLog(@"positiveButtonLabel isEqual sEqual: 0 ");
            }else{
                if ((![positiveButtonLabel isEqualToString:@""]) || ([positiveButtonLabel length] != 0)){
                    UIAlertAction *positiveButton = [UIAlertAction
                                                     actionWithTitle:positiveButtonLabel
                                                     style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                                                     {
                                                         
                                                         NSMutableDictionary * resultDatas = [self.dialogContext getResultData];
                                                         
                                                         [resultDatas setValue:positiveButtonFunction forKey:@"function"];
                                                         
                                                         NSError *error;
                                                         NSData *jsonData = [NSJSONSerialization dataWithJSONObject: resultDatas                         options:NSJSONWritingPrettyPrinted                                      error:&error];
                                                         
                                                         NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
                                                         
                                                         [bridge returnResult:callbackId synchronously:NO args:jsonArray, nil];
                                                     }];
                    [alertController addAction:positiveButton];
                }
            }
            if ([neutralButtonLabel length] == 0) {
//                NSLog(@"neutralButtonLabel isEqual sEqual: 0 ");
                
            }else{
                if ((![neutralButtonLabel isEqual:@""]) || ([neutralButtonLabel length] != 0)  ){
                    UIAlertAction *neutralButton = [UIAlertAction
                                                    actionWithTitle:neutralButtonLabel
                                                    style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                                                    {
                                                        
                                                        NSMutableDictionary * resultDatas = [self.dialogContext getResultData];
                                                        
                                                        [resultDatas setValue:neutralButtonFunction forKey:@"function"];
                                                        
                                                        NSError *error;
                                                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject: resultDatas                         options:NSJSONWritingPrettyPrinted                                      error:&error];
                                                        
                                                        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
                                                        
                                                        [bridge returnResult:callbackId synchronously:NO args:jsonArray, nil];
                                                        
                                                    }];
                    [alertController addAction:neutralButton];
                }
            }
        }
        
        [self presentViewController:alertController  animated:YES completion:nil];
        
    }else if ([functionName isEqualToString:@"CFLocation.getCurrentPosition"]) {
        self.currentLocationRetriever.updateLocation = YES;

        [self.currentLocationRetriever updateCurrentLocation:^(CLLocation* location) {
            NSDictionary* pos = [LocationCoder dictionaryFromLocation:location];
            [bridge returnResult:callbackId synchronously:NO args:pos, nil];
        }];
        
    } else if ([functionName isEqualToString:@"CFLocation.startTracking"]) {
        NSString* url = arguments[0];

        [CFHttpResponseHelper sharedInstance].delegate = self;
        [[BackgroundLocationRetriever instance] startLocationUpdates:url];
        
    } else if ([functionName isEqualToString:@"CFLocation.startTrackingAndSendLocationToServer"]) {
        
        startTrackingUrl  = arguments[0];
        NSString* seconds = arguments[1];
        
//        NSLog(@"SecondsdoubleValue %f ", [seconds doubleValue]);
        [CFHttpResponseHelper sharedInstance].delegate = self;
        
        self.locationTracker = [[LocationTracker alloc]init];
        [self.locationTracker startLocationTracking];
        
        //Send the best location to server every NSString* seconds  seconds
        //You may adjust the time interval depends on the need of your app.
        NSTimeInterval time = [seconds doubleValue];
        self.locationUpdateTimer =
        [NSTimer scheduledTimerWithTimeInterval:time
                                         target:self
                                       selector:@selector(updateLocation)
                                       userInfo:nil
                                        repeats:YES];
        
    } else if ([functionName isEqualToString:@"CFLocation.stopTracking"]) {
        [[BackgroundLocationRetriever instance] stopLocationUpdates];
        
    } else if ([functionName isEqualToString:@"CFLocation.getLocationQueue"]) {
        NSArray* locations = [[BackgroundLocationRetriever instance] getQueuedLocations];
        NSMutableArray* result = [NSMutableArray arrayWithCapacity:locations.count];
        for (CLLocation* location in locations) {
            [result addObject:[LocationCoder dictionaryFromLocation:location]];
        }
        [bridge returnResult:callbackId synchronously:YES args:result, nil];
        
    } else if ([functionName isEqualToString:@"CFLocation.addFence"]) {
        NSDictionary* fence = arguments[0];
        
        CLLocationDistance radius = [((NSNumber*) fence[@"radius"]) doubleValue];
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [((NSNumber*) fence[@"lat"]) doubleValue ];
        coordinate.longitude = [((NSNumber*) fence[@"long"]) doubleValue];
        NSString* identifier = fence[@"id"];
        
        CLCircularRegion* region = [[CLCircularRegion alloc] initWithCenter:coordinate radius:radius identifier:identifier];
        
        [[LocationFencer instance] addObservedRegion:region];
        
    } else if ([functionName isEqualToString:@"CFLocation.getFences"]) {
        NSSet* regions = [LocationFencer instance].monitoredRegions;
        NSMutableArray* fences = [[NSMutableArray alloc] initWithCapacity:regions.count];
        for (CLRegion* region in regions) {
            if ([region isKindOfClass:[CLCircularRegion class]]) {
                CLCircularRegion* circularRegion = (CLCircularRegion*) region;
                NSDictionary* fence = [LocationCoder dictionaryFromCircularRegion:circularRegion];
                [fences addObject:fence];
            }
        }
        
        [bridge returnResult:callbackId synchronously:YES args:fences, nil];
    } else if ([functionName isEqualToString:@"CFLocation.removeFence"]) {
        NSString* regionName = arguments[0];
        
        [[LocationFencer instance] removeObservedRegion:regionName];
    } else if ([functionName isEqualToString:@"CFLocation.setFencingUrl"]) {
        NSString* url = arguments[0];
        [BundleSettings setFencingUrl:url];
        
        
    }else if ([functionName isEqualToString:@"CFLocation.setData"]) {
        
        // NSLog(@"sfdsfsdfdsfsdf %@", [CFHttpRequest sharedInstance].response.body );
        //        NSString *data = arguments[0];
        //
        //        [bridge returnResult:callbackId synchronously:NO args:data, nil];
        
    } else if ([functionName isEqualToString:@"CFNotification.addObserverForName"]) {
        NSString* notificationName = arguments[0];
        
//        NSLog(@"Pavel - CFNotification.addObserverForName - notification %@", notificationName);
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [notifications addObject:[center addObserverForName:notificationName
                                                     object:nil
                                                      queue:nil
                                                 usingBlock:^(NSNotification *notification)
                                  {
//                                      NSLog(@"%@ callbackid: %ld", notification.name, (long)callbackId);
                                      [bridge returnResult:callbackId synchronously:NO args: nil];
                                  }]];
    } else if ([functionName isEqualToString:@"CFNotification.removeObserverForName"]) {
        NSString* notificationName = arguments[0];
        
//        NSLog(@"Pavel - CFNotification.removeObserverForName - notification %@", notificationName);
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center removeObserver:self name:notificationName object:nil];
    } else if ([functionName isEqualToString:@"CFNotification.postNotificationWithName"]) {
        NSString* notificationName = arguments[0];
        
//        NSLog(@"Pavel - CFNotification.postNotificationWithName - notification %@", notificationName);
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:notificationName object:nil];
    } else if ([functionName isEqualToString:@"CFPictureChooser.capturePicture"]) {
        UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
        
        imagePicker.sourceType = [arguments[0] isEqualToString:@"PHOTO"] ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        _imagePickerDataResult = [arguments[1] isEqualToString:@"DATA"];
        
        if ([UIImagePickerController isSourceTypeAvailable:imagePicker.sourceType]) {
            imagePicker.mediaTypes = @[(NSString*) kUTTypeImage];
            imagePicker.delegate = self;
            imagePicker.allowsEditing = NO;
            _imagePickerCallbackId = callbackId;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
            
        }
    } else if ([functionName isEqualToString:@"CFPictureChooser.getCapturedPictures"]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *imageDirectory = [[paths objectAtIndex:0] stringByAppendingString:@"/pictures"];
        
        NSFileManager* fileManager = [NSFileManager defaultManager];
        NSDirectoryEnumerator *dirEnum = [fileManager enumeratorAtPath:imageDirectory];
        
        NSMutableArray* resultArray = [NSMutableArray array];
        
        NSString *file;
        while ((file = [dirEnum nextObject])) {
            if ([[file pathExtension] isEqualToString: @"jpg"]) {
                [resultArray addObject:[NSURL fileURLWithPath:[imageDirectory stringByAppendingPathComponent:file]].absoluteString];
            }
        }
        
        [self.nativeBridge returnResult:callbackId synchronously:NO args:resultArray, nil];
    } else if ([functionName isEqualToString:@"CFPictureChooser.getPictureData"]) {
        NSString* imageUri = arguments[0];
        NSString* imagePath = [[NSURL URLWithString:imageUri] path];
        
        NSData* fileData = [NSData dataWithContentsOfFile:imagePath];
        NSString* result = [fileData base64EncodedStringWithOptions:0];
        
        [self.nativeBridge returnResult:callbackId synchronously:NO args:result, nil];
    } else if ([functionName isEqualToString:@"CFPictureChooser.deletePicture"]) {
        NSString* imageUri = arguments[0];
        NSString* imagePath = [[NSURL URLWithString:imageUri] path];
        
        NSFileManager* fileManager = [NSFileManager defaultManager];
        NSError* error;
        [fileManager removeItemAtPath:imagePath error:&error];
    } else if ([functionName isEqualToString:@"CFPictureChooser.deleteAllPictures"]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *imageDirectory = [[paths objectAtIndex:0] stringByAppendingString:@"/pictures"];
        
        NSFileManager* fileManager = [NSFileManager defaultManager];
        NSDirectoryEnumerator *dirEnum = [fileManager enumeratorAtPath:imageDirectory];
        
        NSError* error;
        NSString *file;
        while ((file = [dirEnum nextObject])) {
            [fileManager removeItemAtPath:[imageDirectory stringByAppendingPathComponent:file] error:&error];
        }
    } else if ([functionName isEqualToString:@"CFIdentity.getDeviceIdentifier"]) {
        
        NSString* identifier = [CFIdentity advertisingIdentifier];
        [self.nativeBridge returnResult:callbackId synchronously:NO args:identifier, nil];
    } else if ([functionName isEqualToString:@"CFRuntime.restartLocalApp"]) {
        [BundleSettings setOverrideRemoteApp:NO];
        [self.webView reload];
        [self initRuntimeFromUrl:nil];
    } else if ([functionName isEqualToString:@"CFRuntime.resetLocalAppFiles"]) {
        [[CFRuntime instance] resetAppFiles];
    } else if ([functionName isEqualToString:@"CFRuntime.getLocalFiles"]) {
        NSArray* files = [[CFRuntime instance] getLocalFilePaths];
        
        NSMutableArray* resultArray = [NSMutableArray arrayWithCapacity:files.count];
        
        [files enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString* file = obj;
//            NSLog(@"%lu: %@ -> %@", (unsigned long)idx, file, [NSURL fileURLWithPath:file]);
            [resultArray addObject:[NSURL fileURLWithPath:file].absoluteString];
        }];
        
        [self.nativeBridge returnResult:callbackId synchronously:NO args:resultArray, nil];
        
    } else if ([functionName isEqualToString:@"CFRuntime.deleteLocalFile"]) {
        
        NSString* fileUrlString = arguments[0];
        NSString* filePath = [[NSURL URLWithString:fileUrlString] path];
        
        NSError* error;
        NSFileManager* fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:filePath error:&error];
        
    } else if ([functionName isEqualToString:@"CFRuntime.addLocalFile"]) {
        
        NSString* targetType = arguments[0];
        NSString* srcUrl = arguments[2];
        NSString* targetUrl = arguments[1];
        
        NSString* targetPath;
        
        // complete target URL
        if ([@"CONTENT" isEqualToString:targetType]) {
            targetPath = [[CFRuntime pathForExtractedLocalAppContent] stringByAppendingPathComponent:targetUrl];
        } else {
            targetPath = [[CFRuntime pathForExtractedApp] stringByAppendingPathComponent:targetUrl];
        }
        
        BOOL success = [[CFRuntime instance] downloadAndReplaceLocalFile:targetPath withContentOfUrl:[NSURL URLWithString:srcUrl]];
        
        [self.nativeBridge returnResult:callbackId synchronously:NO args:@(success), nil];
    } else if ([functionName isEqualToString:@"CFRuntime.setOrientation"]) {
        
        NSString* orientation = arguments[0];
        
        [[CFRuntime instance] setOrientation:orientation];
        
        
    } else if ([functionName isEqualToString:@"CFRuntime.unsetOrientation"]) {
        [[CFRuntime instance] setOrientation:@""];
        
    } else if ([functionName isEqualToString:@"CFShare.shareString"]) {
        
        NSString* stringToShare = arguments[0];
        [CFShare shareString:stringToShare parentViewController:self];
        
    } else if ([functionName isEqualToString:@"CFExternal.showExternalContent"]) {
        
        NSString* title = arguments[0];
        NSString* contentUrl = arguments[1];
        
        ExternalContentViewController* externalController = [[ExternalContentViewController alloc] init];
        externalController.showUrl = @"true";
        externalController.showHtmlFile = @"false";
        externalController.showHtmlString = @"false";
        
        externalController.title = title;
        NSString *encodedString=[contentUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        externalController.url = [NSURL URLWithString:encodedString];
        
        BaseNavigationController* navController = [[BaseNavigationController alloc] initWithRootViewController:externalController];
        
        [self presentViewController:navController animated:YES completion:nil];
        
        
    } else if ([functionName isEqualToString:@"CFExternal.showExternalHtmlFile"]) {
        
        NSString* title = arguments[0];
        NSString* htmlFileName = arguments[1];
        
        ExternalContentViewController* externalController = [[ExternalContentViewController alloc] init];
        
        externalController.showUrl = @"false";
        externalController.showHtmlFile = @"true";
        externalController.showHtmlString = @"false";
        
        externalController.title = title;
        externalController.htmlFileName = htmlFileName;
        
        BaseNavigationController* navController = [[BaseNavigationController alloc] initWithRootViewController:externalController];
        
        [self presentViewController:navController animated:YES completion:nil];
        
    } else if ([functionName isEqualToString:@"CFExternal.showExternalHtmlString"]) {
        
        NSString* title = arguments[0];
        NSString* htmlString = arguments[1];
        
        ExternalContentViewController* externalController = [[ExternalContentViewController alloc] init];
        
        externalController.showUrl = @"false";
        externalController.showHtmlFile = @"false";
        externalController.showHtmlString = @"true";
        
        externalController.title = title;
        externalController.htmlString = htmlString;
        
        BaseNavigationController* navController = [[BaseNavigationController alloc] initWithRootViewController:externalController];
        
        [self presentViewController:navController animated:YES completion:nil];
        
    } else if ([functionName isEqualToString:@"CFExternal.openUrlInSafari"]) {
        NSString* url = arguments[0];
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:url]]) {
            [[UIApplication sharedApplication] openURL:
             [NSURL URLWithString:url]];
        } else {
            NSString *prefix = nil;
            //Check if it's comgooglemaps
            if ([url length] >= 13){
                prefix = [url substringToIndex:13];
            } else {
                prefix = url;
            }

            if ([prefix isEqual:@"comgooglemaps"]) {
                //Open store
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://itunes.apple.com/app/id585027354"]];
            }else{
//                NSLog(@"Can't use %@ ", url);
            }
        }
    } else if ([functionName isEqualToString:@"CFRuntime.findPage"]) {
        
        NSString* pageName = arguments[0];
        
//        NSLog(@"findPage: %@", pageName);
        
        id<TypedPage> typedPage = [self findPageInCurrentStack:pageName];
        
        [self.nativeBridge returnResult:callbackId synchronously:NO args:typedPage, nil];
        
    } else if ([functionName isEqualToString:@"CFRuntime.findCurrentPage"]) {
        
        CFHtmlPage* htmlPage;
        
        if (!self.typedPage) {
            htmlPage = [[CFHtmlPage alloc] initWithPage:self.navigationService.page];
            self.typedPage = htmlPage;
        } else {
            htmlPage = (CFHtmlPage*) self.typedPage;
        }
        NSString *uniqueId = [[NSString alloc] initWithString:htmlPage.page.uniqueId];
        [bridge returnResult:callbackId synchronously:NO args:uniqueId, nil];
        
    } else if ([functionName isEqualToString:@"CFRuntime.updatePage"]) {
        
        NSString* pageName = arguments[0];
        id pageArgument = arguments[1];
        if (pageArgument == [NSNull null]) {
            pageArgument = nil;
        }
        
//        NSLog(@"updatePageLM : %@: %@", pageName, pageArgument);
        
        id<TypedPage> typedPage = [CFPage fromJSON:pageArgument];
        CFPage* page;
        
        if (!typedPage) {
//            NSLog(@"Restoring original page with id %@", pageName);
        } else {
            page = typedPage.page;
        }
        
        if (page) {
            [page setUniqueId:pageName];
        }
        
        [self updatePage:pageName inCurrentStack:typedPage];
        [self refreshStack];
        
    }else if ([functionName isEqualToString:@"CFPrivacy.getContacts"]) {
        
        self.context = [Contacts sharedInstance];
        [self.context contactScan];
        [self.context getContacts];
        NSArray *contacts = [self.context contacts];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:contacts options:0 error:nil];
        
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
        //    NSString *name = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        NSLog(@"jsonArray %@", jsonArray);
        [bridge returnResult:callbackId synchronously:NO args:jsonArray, nil];
        
        
    }else if ([functionName isEqualToString:@"CFPrivacy.getContactsDataByIds"]) {
        
        NSString* contactId = arguments[0];
        
        self.context = [Contacts sharedInstance];
        [self.context getContactDetails:contactId];
        NSArray *contacts = [self.context contactDetails];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:contacts options:0 error:nil];
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
        
        [bridge returnResult:callbackId synchronously:NO args:jsonArray, nil];
    }else if ([functionName isEqualToString:@"CFPrivacy.checkPermission"]) {
        NSString *type = arguments[0];
        if ([type isEqualToString:@"location"]) {
            [self.currentLocationRetriever checkPermission:^(BOOL status) {
                if (status) {
                    [bridge returnResult:callbackId synchronously:NO args:@YES, nil];
                } else {
                    [bridge returnResult:callbackId synchronously:NO args:@NO, nil];
                }
            }];
        } else if ([type isEqualToString:@"notification"]) {
            if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
                [bridge returnResult:callbackId synchronously:NO args:@YES, nil];
            } else {
                [bridge returnResult:callbackId synchronously:NO args:@NO, nil];
            }
        } else {
            //TODO: do it for camera, storage...
            //granted
            [bridge returnResult:callbackId synchronously:NO args:@YES, nil];
            
        }

    }else if ([functionName isEqualToString:@"CFPrivacy.requestPermission"]) {
        NSString *type = arguments[0];
        if ([type isEqualToString:@"location"]) {
            [self.currentLocationRetriever requestPermission:^(NSString * pemissionStatus) {

                if ([pemissionStatus isEqualToString:@"whenInUse"] || [pemissionStatus isEqualToString:@"alwaysAllow"]) {

                    //granted
                    [bridge returnResult:callbackId synchronously:NO args:@YES, nil];

                }else{
                    //not granted
                    [bridge returnResult:callbackId synchronously:NO args:@NO, nil];
                }

            }];
        }else{
            //TODO: do it for camera, storage...
            //granted
            [bridge returnResult:callbackId synchronously:NO args:@YES, nil];
            
        }
    } else if ([functionName isEqualToString:@"CFBadge.update"]) {
        
        NSString *badgeNumber =  [NSString stringWithFormat:@"%@", arguments[0]];
        
        self.badgesContext = [Badges sharedInstance];
        NSString *getBadgeNumber = [self.badgesContext updateBadgeNumber:badgeNumber];
        
        [bridge returnResult:callbackId synchronously:NO args:getBadgeNumber, nil];
        
        [DefaultNavigationImpl updateBadgeNew:self.navigationController];
        
    }else if ([functionName isEqualToString:@"CFBadge.get"]) {
        
        self.badgesContext = [Badges sharedInstance];
        NSString *getBadgeNumber = [self.badgesContext getBadgeNumber];
//        NSLog(@"Pavel - CFBadge.get - badgeNumber %@, callbackId: %ld", getBadgeNumber, (long)callbackId);
        [bridge returnResult:callbackId synchronously:NO args:getBadgeNumber, nil];
        
    }else if ([functionName isEqualToString:@"CFHeader.updateToggle"]) {
        
        NSString *data = arguments[0];
        BOOL visibility = [arguments[1] boolValue];
        
        if (visibility) {
            [DefaultNavigationImpl changeTitle:self.navigationController newTitle:data];
        } else {
            [DefaultNavigationImpl clearView:self.navigationController];
        }
        
    }else if ([functionName isEqualToString:@"CFHeader.updateHeader"]) {
        
        NSString *title = arguments[0];
        
        [DefaultNavigationImpl changeTitle:self.navigationController newTitle:title];
        
    }else if ([functionName isEqualToString:@"CFPayPal.singlePayment-DROPPED"]) {
//        _payPalCallbackId = callbackId;
//        NSString *items   = arguments[0];
//        NSString *details = arguments[1];

//        PayPalViewController* viewController = [[PayPalViewController alloc] initWithItems:items andDetails:details];
//        [viewController viewDidLoad];
//        [viewController viewWillAppear:YES];
//        viewController.delegate = self;

//        PayPalPayment *payment = [viewController payment];
//        // Create a PayPalPaymentViewController.
//        PayPalPaymentViewController *paymentViewController;
//        paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
//                                                                       configuration:self.payPalConfig
//                                                                            delegate:self];
//        //Present the PayPalPaymentViewController.
//        [self presentViewController:paymentViewController animated:YES completion:nil];
    }else if ([functionName isEqualToString:@"CFAnalytics.registerPage"]) {
        
        NSString *pageName = arguments[0];
        
        GoogleAnalyticsViewController *googleAnalytics = [[GoogleAnalyticsViewController alloc] init];
        [googleAnalytics registerPage:pageName];
        
    }else if ([functionName isEqualToString:@"CFAnalytics.sendEvent"]) {
        
        NSString *category = arguments[0];
        NSString *action   = arguments[1];
        NSString *label    = arguments[2];
        NSString *value    = arguments[3];
        
        GoogleAnalyticsViewController *googleAnalytics = [[GoogleAnalyticsViewController alloc] init];
        [googleAnalytics sendEventWithCategory:category andAction:action andLabel:label andValue:value];
        
    }else if ([functionName isEqualToString:@"CFAnalytics.sendProduct"]) {
        
        NSString *transactionId   = arguments[0];
        NSString *name            = arguments[1];
        NSString *productCategory = arguments[2];
        NSString *price           = arguments[3];
        NSString *quantity        = arguments[4];
        NSString *currencyCode    = arguments[5];
        NSString *sku             = arguments[6];
        
        GoogleAnalyticsViewController *googleAnalytics = [[GoogleAnalyticsViewController alloc] init];
        [googleAnalytics sendProductWithTransaction:transactionId andName:name andSku:sku andCategory:productCategory andPrice:price andQuantity:quantity andCurrencyCode:currencyCode];
        
//        NSLog(@"CFAnalytics.sendProduct");
        
    }else if ([functionName isEqualToString:@"CFAnalytics.sendTransaction"]) {
        
        NSString *transactionTransactionId   = arguments[0];
        NSString *affiliation                = arguments[1];
        NSString *revenue                    = arguments[2];
        NSString *tax                        = arguments[3];
        NSString *shipping                   = arguments[4];
        NSString *transactionCurrencyCode    = arguments[5];
        
        GoogleAnalyticsViewController *googleAnalytics = [[GoogleAnalyticsViewController alloc] init];
        [googleAnalytics sendTransactionWithTransaction:transactionTransactionId andAffiliation:affiliation andRevenue:revenue andTax:tax andShipping:shipping andCurrencyCode:transactionCurrencyCode];
    }else if ([functionName isEqualToString:@"SmartLink.start"]) {
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        reachability.delegate = self;
        
        SCNetworkReachabilityFlags flags = [reachability reachabilityFlags];
        
        [reachability reachabilityChanged:flags];
        
        NetworkStatus status = [reachability currentReachabilityStatus];
        if(status == NotReachable)
        {
//            NSLog(@" No connection");
        }
        else
        {
            
            NSString *ssid     = arguments[0];
            NSString *password = arguments[1];
            NSNumber *timeout  = arguments[2];
            
            [SmartLink sharedInstance].delegate = self;
            
            [[SmartLink sharedInstance] connectSsid:ssid andPassword:password andTiomeout:timeout];
            [[SmartLink sharedInstance] connectPress];
            
        }
    }else if ([functionName isEqualToString:@"CFNetwork.getCurrentWifi"]) {
        
        NSDictionary *ssidDict = [[SmartLink sharedInstance] fetchSSIDInfo];
        NSString *ssid = ssidDict[@"SSID"];
        [bridge returnResult:callbackId synchronously:NO args:ssid, nil];
        
    }else if ([functionName isEqualToString:@"CFNativeComponent.wirelessSettings"]) {
        
        //  NSURL *URL = [NSURL URLWithString:@"prefs:root=Apps&path=Your+App+Display+Name"];
        //  UIApplication *application = [UIApplication sharedApplication];
        
        //        NSDictionary *options = @{UIApplicationOpenURLOptionUniversalLinksOnly : @YES};
        //        [application openURL:URL options:options completionHandler:nil];
        
    }else if ([functionName isEqualToString:@"CFNativeComponent.vibratePhone"]) {
        
        // TapTic Feedback
        [[UIDevice currentDevice] valueForKey:@"_feedbackSupportLevel"];
        //        UIImpactFeedbackGenerator *myGen = [[UIImpactFeedbackGenerator alloc] init];
        //        [myGen initWithStyle:UIImpactFeedbackStyleLight];
        //        [myGen impactOccurred];
        //        myGen = NULL;
        
    } else if ([functionName isEqualToString:@"CFRuntime.rightToLeftLayout-DISABLED"]) {
        
        NSString* stringValue = arguments[0];
//        NSLog(@"Menu on %@",stringValue);
        
        [[MenuPositionHelper sharedInstance] setMenuPosition:stringValue];
        
        if ([stringValue isEqual:@"right"]) {
            
//            NSLog(@"Menu on RIGHT - %@",stringValue);
            
            //     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
            [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
            
            [[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setSemanticContentAttribute:UISemanticContentAttributeUnspecified];
            [[UIView appearanceWhenContainedIn:[UIAlertView class], nil] setSemanticContentAttribute:UISemanticContentAttributeUnspecified];
            
            
        }
        if ([stringValue isEqual:@"left"]) {
            
            [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
            
            [[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setSemanticContentAttribute:UISemanticContentAttributeUnspecified];
            [[UIView appearanceWhenContainedIn:[UIAlertView class], nil] setSemanticContentAttribute:UISemanticContentAttributeUnspecified];
            
        }
        
        
        CFHtmlPage* htmlPage;
        
        if (!self.typedPage) {
            htmlPage = [[CFHtmlPage alloc] initWithPage:self.navigationService.page];
            self.typedPage = htmlPage;
        } else {
            htmlPage = (CFHtmlPage*) self.typedPage;
        }
        
        //SlidingViewController *slidingView = [[SlidingViewController alloc] init];
        
        NSString *uniqueId = [[NSString alloc] initWithString:htmlPage.page.uniqueId];
        CFPage* page = [[CFRuntime instance] getPageWithId:uniqueId];
        
        [self tryMenuNavigation:page withContext:self.navigationService.navigationContext];
    } else if ([functionName isEqualToString:@"CFBeacons.init"]) {
        NSString* apiKey = arguments[0];
//        NSLog(@"apiKey %@",apiKey);
    } else if ([functionName isEqualToString:@"CFDocuments.getAll"]) {
        // self.webView.mediaPlaybackRequiresUserAction = NO;
        // self.webView.allowsInlineMediaPlayback = YES;
        self.webView.allowsLinkPreview = YES;
        // self.webView.allowsPictureInPictureMediaPlayback = YES;
        NSMutableArray* filesArray = [[NSMutableArray alloc] init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains ( NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSDirectoryEnumerator *de = [[NSFileManager defaultManager] enumeratorAtPath:documentsDirectory] ;
        NSString *file;
        while ((file = [de nextObject])) {
            if ([file.pathComponents[0]  isEqual:@"www"] || [file.pathComponents[0]  isEqual:@"content"] || [file.pathComponents[0]  isEqual:@"storage"] || [file.pathComponents[0]  isEqual:@"storage.bin"]) {
                // NSLog(@"pathComponents %@", file.pathComponents[0]);
            } else {
                NSString *filepath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, file];
                [filesArray addObject:filepath];
            }
        }
        //return all files in Document folder, without files in www, content and storage.bin
        [bridge returnResult:callbackId synchronously:NO args:filesArray, nil];
    } else {
        // unsupported call
//        NSLog(@"nativeBridge handleCall withArguments callbackId forWebView --- unsupported call ---");
    }
}


- (void) saveToCalendar:(NSString*) title date:(NSDate*) date duration:(NSTimeInterval) duration resultBlock:(void (^)(BOOL result)) resultBlock
{
    EKEventStore* store = [[EKEventStore alloc] init];
//    NSLog(@"EKEventStore %@", store);
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (error) {
//            NSLog(@"Error accessing: %@", error);
            if (resultBlock) {
                resultBlock(NO);
            }
        }
        else if (granted) {
            EKEvent* newEvent = [EKEvent eventWithEventStore:store];
            
//            NSLog(@"newEvent %@", newEvent);
            
            newEvent.title = title;
            newEvent.startDate = date;
            newEvent.endDate = [newEvent.startDate dateByAddingTimeInterval:duration];
            newEvent.calendar = store.defaultCalendarForNewEvents;
            [store saveEvent:newEvent span:EKSpanThisEvent commit:YES error:&error];
            NSString *savedEventId = newEvent.eventIdentifier;
//            NSLog(@"savedEventId %@", savedEventId);
            
            //store sa
            if (error) {
//                NSLog(@"Error saving: %@", error);
                if (resultBlock) {
                    resultBlock(NO);
                }
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"calshow:%ld", (long)[date timeIntervalSinceReferenceDate]]]];
                if (resultBlock) {
                    resultBlock(YES);
                }
            }
        } else {
            if (resultBlock) {
                resultBlock(NO);
            }
        }
    }];
}


- (void)dealloc {
    // remove all observers
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self];
}

#pragma Lifecycle

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setIsActive:YES];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self setIsActive:NO];
}

- (void) setIsActive:(BOOL) status {

    if (!_nativeBridgeLoaded) {
        // callback not yet possible; value will be set when native bridge is loaded
        return;
    }
    if (status != _isActive) {
        if (status) {
            [self.nativeBridge invoke:@"onIsActive" waitUntilDone:NO args:nil];
        } else {
            [self.nativeBridge invoke:@"onIsInActive" waitUntilDone:NO args:nil];
        }
    }
    _isActive = status;
}

#pragma mark NavigationViewControllerOverrides

- (void) onResultReceived:(id)result {
    [self.nativeBridge invoke:@"onNavigationResult" waitUntilDone:NO args:[NSString stringWithFormat:@"'%@'", result], nil];
}

#pragma mark ScannerViewControllerDelegate

- (void) scanner:(ScannerViewController*) viewController didCaptureCode:(NSString*) code
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.nativeBridge returnResult:_scannerCallbackId synchronously:NO args:code, nil];
    }];
}

#pragma mark ButtonTarget

- (void) handleRightButtonPress:(id) sender
{
    NSString*function;
    NSArray *rightButtonArray = [CFRuntime instance].rightButtons;

    for (CFHeaderButton *headerButton in rightButtonArray) {
        if ([[sender title] isEqual:headerButton.text]) {
            function = headerButton.function;
        }
        if ([[sender image] isEqual:headerButton.icon]) {
            function = headerButton.function;
        }
    }

//    NSLog(@"FUNCTION IS ---------> %@", function);
    [self.nativeBridge invoke:function waitUntilDone:NO args:nil];
}
- (void) handleContentViewPress:(id) sender
{
    UIButton *button = (UIButton*)sender;
    [button setSelected:!button.isSelected];
    NSString * booleanString = (button.isSelected) ? @"true" : @"false";
    
    CFRuntime *runtime =  [CFRuntime instance];
    UIColor *contentViewColor = runtime.contentViewButton.color;
    
    if (contentViewColor) {
        [button setTitleColor:contentViewColor forState:UIControlStateNormal];
        button.tintColor = contentViewColor;
    }
    
    if (button.isSelected) {
        UIImage*  image = [[UIImage imageNamed:@"arrow-up.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        [button setImage:image forState:UIControlStateNormal];
        [self.nativeBridge invoke:@"toggleClicked" waitUntilDone:NO args: booleanString,nil];
    } else {
        UIImage*  image = [[UIImage imageNamed:@"arrow-down.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [button setImage:image forState:UIControlStateNormal];
        [self.nativeBridge invoke:@"toggleClicked" waitUntilDone:NO args:booleanString, nil];
    }
}

- (void) handleLeftButtonPress:(id) sender {
    if ([self.leftButton.function isEqualToString:@"onBackPressed"]) {
        [self.nativeBridge invoke:self.leftButton.function waitUntilDone:NO args:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) handleBackButtonPress {
    [self.nativeBridge invoke:@"backButtonPressed" waitUntilDone:NO args:nil];
}

#pragma mark CFHttpRequestDelegate

- (void) requestCompleted:(CFHttpRequest*) request
{
    NSDictionary* resultObject;
    if (request.response) {
        NSMutableDictionary* res = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%ld", (long)request.response.code] forKey:@"Code"];
        if (request.response.body) {
            [res setObject:request.response.body forKey:@"Body"];
        }
        resultObject = res;
    }
    [self.nativeBridge returnResult:request.callbackId synchronously:NO args:resultObject, nil];
    
//    NSLog(@"Pavel - requestCompleted");
}

- (void) locationUpdated:(NSNotification*) notification
{
    CLLocation* location = notification.object;
    [self.nativeBridge invoke:@"onLocationUpdated" waitUntilDone:NO args:[LocationCoder dictionaryFromLocation:location], nil];
}

- (void) regionEntered:(NSNotification*) notification
{
    CLRegion* region = notification.object;
    if ([region isKindOfClass:[CLCircularRegion class]]) {
        [self.nativeBridge invoke:@"onFenceEntered" waitUntilDone:NO args:[LocationCoder dictionaryFromCircularRegion:(CLCircularRegion*) region], nil];
    }
}

- (void) regionExited:(NSNotification*) notification
{
    CLRegion* region = notification.object;
    if ([region isKindOfClass:[CLCircularRegion class]]) {
        [self.nativeBridge invoke:@"onFenceExited" waitUntilDone:NO args:[LocationCoder dictionaryFromCircularRegion:(CLCircularRegion*) region], nil];
    }
}

- (void) pushMessageReceived:(NSNotification*) notification
{
    NSDictionary* userInfo = notification.userInfo;
//    NSLog(@"userInfo %@", userInfo);
    //  NSString *value=[[[userInfo valueForKey:@"payload"] objectAtIndex:0] valueForKey:@"target_url"];
    
    NSDictionary *payload = [userInfo valueForKeyPath:@"payload"];
    
    //  NSLog(@"valuevaluevalue %@", value);
    
//    NSLog(@"payload %@", payload);
    
    NSDictionary *data = [payload valueForKeyPath:@"data"];
    NSString *target_url = [data valueForKeyPath:@"target_url"];
    
    if (target_url && ![target_url isEqual:@""]) {
//        NSLog(@"targetUrlvalue is  %@", target_url);
        ExternalContentViewController* externalController = [[ExternalContentViewController alloc] init];
        externalController.showUrl = @"true";
        externalController.showHtmlFile = @"false";
        externalController.showHtmlString = @"false";
        
        externalController.title = @"";
        externalController.url = [NSURL URLWithString:target_url];
        BaseNavigationController* navController = [[BaseNavigationController alloc] initWithRootViewController:externalController];
        [self presentViewController:navController animated:YES completion:nil];
    }
    
    [self.nativeBridge invoke:@"onPushMessageReceived" waitUntilDone:NO args:userInfo, nil];
}

- (void) nativeBridgeLoaded:(NSNotification*) notification
{
    self.nativeBridgeLoaded = YES;
    // when viewDidAppear is first called the native bridge is not yet loaded, so wait until it's loaded an resend the active event
    [self setIsActive:YES];
    
//    NSLog(@"Pavel - nativeBridgeLoaded");
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationUpdated:) name:NOTIFICATION_LOCATION_UPDATE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(regionEntered:) name:NOTIFICATION_GEO_FENCE_ENTRY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(regionExited:) name:NOTIFICATION_GEO_FENCE_EXIT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushMessageReceived:) name:NOTIFICATION_MESSAGE_OPENED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nativeBridgeLoaded:) name:NOTIFICATION_NATIVE_BRIDGE_LOADED object:nil];
    
//    NSLog(@"Pavel - viewWillAppear");
    
    if ([SlidingTabsHelper sharedInstance].slidingTabs) {
        self.navigationController.navigationBar.hidden = YES;
    }
    
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_LOCATION_UPDATE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_GEO_FENCE_ENTRY object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_GEO_FENCE_EXIT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_MESSAGE_OPENED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_NATIVE_BRIDGE_LOADED object:nil];
    
//    NSLog(@"Pavel - viewWillDisappear");
    
    // check if the back button was pressed
    if (self.isMovingFromParentViewController) {
        [self handleBackButtonPress];
    }
}

#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *originalImage, *editedImage, *imageToSave;
    NSURL* mediaUrl;

    editedImage = (UIImage *) [[info objectForKey:UIImagePickerControllerEditedImage] fixOrientation];
    originalImage = (UIImage *) [[info objectForKey:UIImagePickerControllerOriginalImage] fixOrientation] ;
    mediaUrl = (NSURL*) [info objectForKey:UIImagePickerControllerMediaURL];
    if (!mediaUrl) {
        mediaUrl = (NSURL*) [info objectForKey:UIImagePickerControllerReferenceURL];
    }
    
    if (editedImage) {
        imageToSave = editedImage;
    } else {
        imageToSave = originalImage;
    }
    NSData* imageData = UIImageJPEGRepresentation(imageToSave, 0.5);
    
    NSString* result;
    NSError* error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *imageDirectory = [[paths objectAtIndex:0] stringByAppendingString:@"/pictures"];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:imageDirectory isDirectory:nil]) {
        BOOL created = [fileManager createDirectoryAtPath:imageDirectory withIntermediateDirectories:YES attributes:nil error:&error];
        if (!created) {
//            NSLog(@"Failed to create directory %@", imageDirectory);
        }
    }
    
    NSString *imagePath =[imageDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[[NSUUID UUID] UUIDString]]];
    
//    NSLog((@"pre writing to file"));
    if (![imageData writeToFile:imagePath atomically:NO])
    {
//        NSLog(@"Failed to cache image data to disk");
    } else {
//        NSLog(@"the cachedImagedPath is %@", imagePath);
        result = [[NSURL fileURLWithPath:imagePath] absoluteString];;
    }
    
    if (_imagePickerDataResult) {
        result = [imageData base64EncodedStringWithOptions:0];
    }
    
    [self.nativeBridge returnResult:_imagePickerCallbackId synchronously:NO args:result, nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.nativeBridge returnResult:_imagePickerCallbackId synchronously:NO args:nil];
}

#pragma mark ExternalContentViewController

- (void)onDismissClicked:(UIBarButtonItem*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark PayPalPaymentDelegate methods

//- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
//    [self verifyCompletedPayment:completedPayment];
//
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[_payPalResult valueForKey:@"payPalResponse"]  options:0 error:nil];
//    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
//
//    [self dismissViewControllerAnimated:YES completion:^{
//        [self.nativeBridge returnResult:_payPalCallbackId synchronously:NO args:jsonArray, nil];
//    }];
//}
//
//- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//- (void)verifyCompletedPayment:(PayPalPayment *)completedPayment {
//    //Confirms
//    NSData *confirmation = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation
//                                                           options:0
//                                                             error:nil];
//
//    NSDictionary *confirmationDict = [NSJSONSerialization JSONObjectWithData:confirmation options:kNilOptions error:nil];
//
//    NSMutableDictionary *mDict     = [[NSMutableDictionary alloc]init];
//    NSMutableArray *keys           = [[NSMutableArray alloc]init];
//    NSMutableArray *obj            = [[NSMutableArray alloc]init];
//
//    keys = [[mDict allKeys] mutableCopy];
//    obj  = [[mDict allValues] mutableCopy];
//
//    [keys addObjectsFromArray:[confirmationDict allKeys]];
//    [obj addObjectsFromArray:[confirmationDict allValues]];
//
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjects:obj forKeys:keys];
//
//    //Items
//    NSDictionary *itemsDict;
//    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
//
//    for (PayPalItem *item in completedPayment.items) {
//        NSString *itemQuantity = [NSString stringWithFormat:@"%lu",(unsigned long)item.quantity];
//
//        NSDictionary *  itemDict = [[NSDictionary alloc] initWithObjectsAndKeys:itemQuantity, @"quantity",item.name,@"name", item.price, @"price", item.currency, @"currency", item.sku, @"sku", nil];
//        [itemsArray addObject:itemDict];
//    }
//
//    itemsDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:itemsArray, @"items", nil];
//
//    //Details
//    NSDictionary *paymantDetails = [[NSDictionary alloc] initWithObjectsAndKeys:completedPayment.paymentDetails.shipping, @"shipping",completedPayment.paymentDetails.subtotal, @"subtotal", completedPayment.paymentDetails.tax, @"tax", nil];
//
//    NSMutableDictionary *paymentItems = [[NSMutableDictionary alloc] initWithObjectsAndKeys:completedPayment.amount, @"amount", completedPayment.currencyCode, @"currency_code", paymantDetails, @"details", completedPayment.shortDescription, @"short_description", nil];
//
//    NSString *intentItem = [NSString stringWithFormat:@"%lu",(unsigned long)completedPayment.intent];
//
//    if([intentItem isEqual:@"0"])
//    {
//        intentItem = @"sale";
//    }
//
//    [paymentItems setValue:itemsDict forKey:@"item_list"];
//    [paymentItems setValue:intentItem forKey:@"intent"];
//
//    NSMutableDictionary *payPalResponse = [[NSMutableDictionary alloc] initWithObjectsAndKeys:dict, @"payment_details",  paymentItems, @"payment_items",nil];
//
//    _payPalResult =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:payPalResponse, @"payPalResponse",nil];
//}

#pragma Reachability
- (void) callFunctionWithArg:(NSString *)arg {
    NSString *ssid;
    if ([arg isEqual:@"WiFi"]) {
        NSDictionary *ssidDict = [[SmartLink sharedInstance] fetchSSIDInfo];
        ssid = ssidDict[@"SSID"];
    } else {
        ssid = nil;
    }
    [self.nativeBridge invoke:@"onConnectionChanged" waitUntilDone:NO args:[NSString stringWithFormat:@"'%@'", ssid], nil];
}

#pragma SmartLink
- (void) onTimeOut:(NSString *)timeOut {
    [self.nativeBridge invoke:@"onTimeOut" waitUntilDone:NO args:timeOut, nil];
}

- (void) onCompleted:(NSString *)onCompleted {
    [self.nativeBridge invoke:@"onCompleted" waitUntilDone:NO args:[NSString stringWithFormat:@"'%@'", onCompleted], nil];
}

- (void) onLinked:(NSDictionary *)args {
    [self.nativeBridge invoke:@"onLinked" waitUntilDone:NO args:args, nil];
}

- (void) getResponseFromPositionUpdate:(NSString *) response{
    [self.nativeBridge invoke:@"getResponseFromPositionUpdate" waitUntilDone:NO args:response, nil];
}

- (void) handleNotification :(NSDictionary *) response{
    [self.nativeBridge invoke:@"handleNotification" waitUntilDone:NO args:response, nil];
}

- (void) notificationWithUserInfo:(NSDictionary *)userInfo {
    [CFHttpResponseHelper sharedInstance].delegate = self;
    [[CFHttpResponseHelper sharedInstance] sethandleNotification:userInfo];
}
-(void) push:(NSDictionary *)args {
    [self.nativeBridge invoke:@"handleNotification" waitUntilDone:NO args:args, nil];
}

- (void) didReceiveNotification:(NSNotification*)notification
{
    
    [self.nativeBridge invoke:@"onPushMessageReceived" waitUntilDone:NO args:notification.userInfo, nil];

//    NSLog(@"userInfo: %@", notification.userInfo);
    
//    NSLog(@"Pavel - didReceiveNotification");
}

/**
 Required to handle local notifications triggered by beacons
 */
//- (void) application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
//{
////    NSLog(@"Before handle notification");
//    [BIBeaconService handleNotification:notification];
////    NSLog(@"After handle notification");
//}



//Update Location
-(void) updateLocation {
//    NSLog(@"Pavel - updateLocation");
    [self.locationTracker updateLocationToServer:startTrackingUrl];
}



//Facebook
//- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results
//{
//    NSLog(@"app invite result: %@", results);
//    BOOL complete = [[results valueForKeyPath:@"didComplete"] boolValue];
//}
//- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error
//{
//    NSLog(@"app invite error: %@", error.localizedDescription);
//}



#pragma mark AppDownloadViewHelperDelegate

- (void) reloadWithUrl:(NSString *)urlString {
    AppDownloadViewController* viewController = [[AppDownloadViewController alloc] init];
    viewController.delegate = self;
    BaseNavigationController* navigation = [[BaseNavigationController alloc] initWithRootViewController:viewController];
    
//    NSLog(@"Pavel - reloadWithUrl");
    
    [self presentViewController:navigation animated:NO completion:nil];
}

- (void) removeLocalStorage {
    [_storage removeValue];
    [_dataPool removeValue];
}
- (void)webView:(WKWebView *)webView didFailLoadWithError:(NSError *)error
{
    //[self stopWaitingAnimaiton];
//    NSLog(@"Failed to load with error web: %@", [error debugDescription]);
//    NSLog(@"Pavel - webView didFailLoadWithError - %@", [error debugDescription]);
}



#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    // Create the request.
    //  NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]];
    
    // Create url connection and fire request
    //   NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    _responseData = [[NSMutableData alloc] init];
    
//    NSLog(@"Pavel - connection didReceiveResponse");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
    
//    NSLog(@"Pavel - connection didReceiveData");
    
//    NSLog(@"_responseData %@", _responseData);
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
//    NSLog(@"Pavel - connectionDidFinishLoading");
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    
//    NSLog(@"Pavel - connection didFailWithError - %ld", (long)error.code);
}

- (NSDictionary *) getDataFrom:(NSString *)url{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
//    NSLog(@"Pavel - getDataFrom - %ld", (long)[responseCode statusCode]);
    
    if([responseCode statusCode] != 200){
//        NSLog(@"Error getting %@, HTTP status code %li", url, (long)[responseCode statusCode]);
        return nil;
    }
    return [NSJSONSerialization JSONObjectWithData:oResponseData options:kNilOptions error:&error];
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    return topController;
}

- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    
    
//    NSString *htmlStr = @"<!DOCTYPE html><html><body><button type=\"button\" onclick=\"myFunction()\">Click Me!</button><script type=\"text/javascript\">function myFunction() { window.webkit.messageHandlers.log.postMessage('CFRuntime.log()'); }</script></body></html>";
    
    //if ([message.name isEqualToString:@"log"])
//        NSLog(@"Pavel - message.body %@", message.body);
}



//#pragma mark - WKUIDelegate
//
//- (void)                        webView:(WKWebView *)webView
//  runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt
//                            defaultText:(NSString *)defaultText
//                       initiatedByFrame:(WKFrameInfo *)frame
//                      completionHandler:(void (^)(NSString * _Nullable))completionHandler
//{
//    NSLog(@"Pavel - prompt, %@", prompt);
//
//    NSData *dataFromString = [prompt dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
//
//    NSDictionary *passedObject = [NSJSONSerialization JSONObjectWithData:dataFromString options:NSJSONReadingMutableContainers error:nil];
//    NSString *type = [passedObject objectForKey:@"type"];
//
//    if ([type isEqualToString:@"SJbridge"]) {
//        self.completionHandler = completionHandler;
//        [NSTimer scheduledTimerWithTimeInterval:5.0
//                                         target:self
//                                       selector:@selector(doSomethingWhenTimeIsUp:)
//                                       userInfo:nil
//                                        repeats:NO];
//    } else {
//        completionHandler(@"Unhandled prompt");
//    }
//
//    NSLog(@"Pavel - prompt, finished");
//}
//
//- (void) doSomethingWhenTimeIsUp:(NSTimer*)t {
//    NSLog(@"Pavel - prompt, doSomethingWhenTimeIsUp");
//    if (self.completionHandler) {
//        self.completionHandler(@"{cartId: 123, items: 1, name=\"Processed Items\"}");
//    }
//}

@end
