//
//  SlidingTabsHelper.m
//  app
//
//  Created by LeyLa on 5/25/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import "SlidingTabsHelper.h"

@implementation SlidingTabsHelper


+ (SlidingTabsHelper *)sharedInstance {
    static dispatch_once_t onceToken;
    static SlidingTabsHelper *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[SlidingTabsHelper alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}
-(void) setSlidingTabsEnable: (NSString *) string {
    self.checkIsEnable = string;
}

@end
