//
//  MenuPositionHelper.m
//  app
//
//  Created by LeyLa Mehmed on 2/09/17.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "MenuPositionHelper.h"
#import "AppDelegate.h"
@implementation MenuPositionHelper


+ (MenuPositionHelper *)sharedInstance {
    static dispatch_once_t onceToken;
    static MenuPositionHelper *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[MenuPositionHelper alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
