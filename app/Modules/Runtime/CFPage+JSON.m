//
//  CFPage+JSON.m
//  app
//
//  Created by Bernhard Kunnert on 24.09.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFPage+JSON.h"
#import "CFTabPage.h"
#import "CFHtmlPage.h"
#import "CFSliderPage.h"
#import "CFSlidingTabPage.h"

@implementation CFPage (JSON)

+ (NSDictionary*) removeNullObjectsFromDictionary:(NSDictionary*) source
{
    NSMutableDictionary* dict = [source mutableCopy];
    [source enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if (obj == [NSNull null]) {
            [dict removeObjectForKey:key];
        }
    }];
    return dict;
}

+ (id<TypedPage>) fromJSON:(NSDictionary *)receivedObjects
{
    if (!receivedObjects) {
        return nil;
    }
    receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];
    id page;
    NSString* type = receivedObjects[@"type"];
    if ([@"tab" isEqualToString:type]) {
        page = [[CFTabPage alloc] initWithJson:receivedObjects];
    } else if ([@"html" isEqualToString:type]) {
        page = [[CFHtmlPage alloc] initWithJson:receivedObjects];
    } else if ([@"navdrawer" isEqualToString:type]) {
        page = [[CFSliderPage alloc] initWithJson:receivedObjects];
    } else if ([@"slidingtab" isEqualToString:type]) {
        page = [[CFSlidingTabPage alloc] initWithJson:receivedObjects];
    }
    return page;
}

@end


