//
//  TypedPage.h
//  app
//
//  Created by Bernhard Kunnert on 01.10.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"

@protocol TypedPage <NSObject>

@property(strong, nonatomic, readonly) CFPage* page;

@end
