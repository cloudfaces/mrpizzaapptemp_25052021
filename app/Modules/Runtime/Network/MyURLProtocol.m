//
//  MyURLProtocol.m
//  app
//
//  Created by LeyLa on 6/19/17.
//  Copyright © 2017 CloudFaces. All rights reserved.
//

#import "MyURLProtocol.h"
@interface MyURLProtocol () <NSURLConnectionDelegate>
@property (nonatomic, strong) NSURLConnection *connection;
@end
@implementation MyURLProtocol

+ (BOOL)canInitWithRequest:(NSURLRequest *)request {
    //static NSUInteger requestCount = 0;
    //NSString*requestStr=  [self formatURLRequest:request];
    return NO;
}

//
//+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {
//    return request;
//}
//
//+ (BOOL)requestIsCacheEquivalent:(NSURLRequest *)a toRequest:(NSURLRequest *)b {
//    return [super requestIsCacheEquivalent:a toRequest:b];
//}
//
//- (void)startLoading {
//    self.connection = [NSURLConnection connectionWithRequest:self.request delegate:self];
//}
//
//- (void)stopLoading {
//    [self.connection cancel];
//    self.connection = nil;
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
//    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
//    [self.client URLProtocol:self didLoadData:data];
//
//    NSString * string = [[NSString alloc] initWithData:data encoding:
//                         NSASCIIStringEncoding];
//
//    if (string.intValue == 1) {
//
//    } else {
//        
//    }
//}
//
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
//    [self.client URLProtocolDidFinishLoading:self];
//}
//
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
//    [self.client URLProtocol:self didFailWithError:error];
//}

+ (NSString *)formatURLRequest:(NSURLRequest *)request
{
    NSMutableString *message = [NSMutableString stringWithString:@"---REQUEST---------Leyla---------\n"];
    [message appendFormat:@"URL: %@\n",[request.URL description] ];
    [message appendFormat:@"METHOD: %@\n",[request HTTPMethod]];
    for (NSString *header in [request allHTTPHeaderFields])
    {
        [message appendFormat:@"%@: %@\n",header,[request valueForHTTPHeaderField:header]];
    }
    [message appendFormat:@"BODY: %@\n",[[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]];
    [message appendString:@"----------------------------\n"];
    return [NSString stringWithFormat:@"%@",message];
}

- (NSString *)formatURLResponse:(NSHTTPURLResponse *)response withData:(NSData *)data
{
    NSString *responsestr = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    NSMutableString *message = [NSMutableString stringWithString:@"---RESPONSE------------------\n"];
    [message appendFormat:@"URL: %@\n",[response.URL description] ];
    [message appendFormat:@"MIMEType: %@\n",response.MIMEType];
    [message appendFormat:@"Status Code: %ld\n",(long)response.statusCode];
    for (NSString *header in [[response allHeaderFields] allKeys])
    {
        [message appendFormat:@"%@: %@\n",header,[response allHeaderFields][header]];
    }
    [message appendFormat:@"Response Data: %@\n",responsestr];
    [message appendString:@"----------------------------\n"];
    return [NSString stringWithFormat:@"%@",message];
}

@end
