//
//  CFHttpRequest.m
//  app
//
//  Created by Bernhard Kunnert on 04.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFHttpRequest.h"
#import "CFHttpResponseHelper.h"
@interface CFHttpRequest () <NSURLConnectionDataDelegate>

@property(nonatomic, strong) NSMutableData* dataToBeLoaded;
@property(nonatomic, strong) NSURLConnection* connection;
@property(nonatomic, strong) CFHttpResponse* response;

@end

@implementation CFHttpRequest

+ (CFHttpRequest *)sharedInstance {
    static dispatch_once_t onceToken;
    static CFHttpRequest *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[CFHttpRequest alloc] init];
    });
    return instance;
}

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        self.dataToBeLoaded = [[NSMutableData alloc] init];
    }
    return self;
}

- (void) send
{
    NSURL *url = [NSURL URLWithString:self.url];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    BOOL contentTypeSet = NO;
    if (self.headers) {
        for (NSString* key in self.headers) {
            id value = [self.headers objectForKey:key];
            [request addValue:value forHTTPHeaderField:key];
            if ([key isEqualToString:@"Content-Type"]) {
                contentTypeSet = YES;
            }
        }
    }
    
    if (!contentTypeSet) {
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    
    [request setHTTPMethod:self.method];
    [request setHTTPBody:[self.body dataUsingEncoding:NSUTF8StringEncoding]];
//    NSLog(@"Sending request: %@", request.description);
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) sendFile
{
    NSURL *url = [NSURL URLWithString:self.url];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    BOOL contentTypeSet = NO;
    if (self.headers) {
        for (NSString* key in self.headers) {
            id value = [self.headers objectForKey:key];
            [request addValue:value forHTTPHeaderField:key];
            if ([key isEqualToString:@"Content-Type"]) {
                contentTypeSet = YES;
            }
        }
    }
    
    if (!contentTypeSet) {
        NSString* contentType = @"application/octet-stream";
        
        if ([self.filePath.pathExtension isEqualToString:@"jpg"] || [self.filePath.pathExtension isEqualToString:@"jpeg"]) {
            contentType = @"image/jpeg";
        } else if ([self.filePath.pathExtension isEqualToString:@"png"]) {
            contentType = @"image/png";
        }
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    }
    
    [request setHTTPMethod:self.method];
    NSData* binaryData = [NSData dataWithContentsOfFile:self.filePath];
    
    [request setHTTPBody:binaryData];
//    NSLog(@"Sending request: %@", request.description);
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

#pragma mark NSURLConnectionDelegage
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"connection did fail: %@", error);
    self.connection = nil;
    [self invokeDelegate];
}

#pragma mark NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//    NSLog(@"Response received: %@", response);
//    NSLog(@"Response received BODYYY: %@", response);
    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
        self.response = [[CFHttpResponse alloc] init];
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*) response;
        self.response.code = httpResponse.statusCode;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.dataToBeLoaded appendData:data];
//    NSLog(@"Data received: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    //Get callback from updating location
    [[CFHttpResponseHelper sharedInstance] setResponse: [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    self.connection = nil;
    if (self.response) {
        self.response.body = [[NSString alloc] initWithData:self.dataToBeLoaded encoding:NSUTF8StringEncoding];
    }
    [self invokeDelegate];
}

- (void) invokeDelegate
{
    if (self.delegate) {
        [self.delegate requestCompleted:self];
    }
}


@end
