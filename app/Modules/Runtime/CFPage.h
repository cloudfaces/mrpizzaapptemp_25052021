//
//  CFPage.h
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPageHeader.h"
#import "BSJsonSupported.h"

@interface CFPage : NSObject <BSJsonSupported>

@property (copy, nonatomic) NSString* uniqueId;
@property (copy, nonatomic) NSString* type;
@property (copy, nonatomic) NSString* title;
@property (strong, nonatomic) CFPageHeader* pageHeader;

@end
