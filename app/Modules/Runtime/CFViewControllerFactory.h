//
//  CFViewControllerFactory.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "Navigation.h"

@interface CFViewControllerFactory : NSObject

- (UIViewController<Navigation>*) viewControllerForPage:(CFPage*) page;
@property (strong, nonatomic) NSMutableDictionary *navStack;

@end
