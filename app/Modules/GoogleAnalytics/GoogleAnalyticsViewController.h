//
//  GoogleAnalyticsViewController.h
//  app
//
//  Created by LeyLa on 4/12/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoogleAnalyticsViewController : UIViewController

//registerPage
@property(nonatomic, strong) NSString *pageName;
- (void) registerPage:(NSString *)pageName;

//send Event
@property(nonatomic, strong) NSString *category;
@property(nonatomic, strong) NSString *action;
@property(nonatomic, strong) NSString *label;
@property(nonatomic, strong) NSString *value;
- (void) sendEventWithCategory:(NSString *)category andAction:(NSString *) action andLabel:(NSString *) label andValue:(NSString *) value;


//send Product
@property(nonatomic, strong) NSString *transactionId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *sku;
@property(nonatomic, strong) NSString *productCategory;
@property(nonatomic, strong) NSString *price;
@property(nonatomic, strong) NSString *quantity;
@property(nonatomic, strong) NSString *currencyCode;

- (void) sendProductWithTransaction:(NSString *)transactionId andName:(NSString *) name andSku:(NSString *) sku andCategory:(NSString *) category andPrice:(NSString *) price andQuantity:(NSString *) quantity andCurrencyCode:(NSString *) currencyCode;

//send Transaction
@property(nonatomic, strong) NSString *transactionTransactionId;
@property(nonatomic, strong) NSString *affiliation;
@property(nonatomic, strong) NSString *revenue;
@property(nonatomic, strong) NSString *tax;
@property(nonatomic, strong) NSString *shipping;
@property(nonatomic, strong) NSString *transactionCurrencyCode;
- (void) sendTransactionWithTransaction:(NSString *)transactionId andAffiliation:(NSString *) affiliation andRevenue:(NSString *) revenue andTax:(NSString *) tax andShipping:(NSString *) shipping andCurrencyCode:(NSString *) currencyCode;
//
////Firebase Analytics
//- (void) setUserProperty:(NSString *) property forName:(NSString *)name;
//- (void) logEventWithName:(NSString *) name;
//- (void) setUserId:(NSString *)userId;
//- (void) handleOpenURL:(NSString *) stringURL;
//- (void) handleEventsForBackgroundURLSession:(NSString *) session;
//- (void) handleUserActivity:(NSString *) activity;
@end
