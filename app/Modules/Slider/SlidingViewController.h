//
//  SlidingViewController.h
//  app
//
//  Created by Bernhard Kunnert on 05.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "SWRevealViewController.h"
#import "Navigation.h"

@interface SlidingViewController : SWRevealViewController <Navigation>
- (void) reloadApp;
@property CFPage *page2;
- (void) setPage:(CFPage *)page;

@end
