//
//  PayPalViewController.h
//  app
//
//  Created by LeyLa on 3/24/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PayPalMobile.h"
#import "BaseViewController.h"


@class PayPalViewController;

@protocol PayPalViewControllerDelegate <NSObject>

@end

@interface PayPalViewController : BaseViewController 

@property (nonatomic, strong, readwrite) NSString *environment;
@property (nonatomic, strong) NSMutableDictionary *payPalResult;
@property (weak, nonatomic) id<PayPalViewControllerDelegate> delegate;

- (instancetype) initWithItems:(NSString*) items andDetails:(NSString *) details;
//- (PayPalPayment *) payment;

@end
