//
//  ExternalContentViewController.h
//  app
//
//  Created by Jakob Haider on 15/09/15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BaseViewController.h"
#import <WebKit/WebKit.h>

@interface ExternalContentViewController : BaseViewController <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

@property (nonatomic, strong) NSURL* url;
@property (nonatomic, strong) NSString* htmlFileName;
@property (nonatomic, strong) NSString* htmlString;

@property (nonatomic, strong) NSString* showUrl;
@property (nonatomic, strong) NSString* showHtmlFile;
@property (nonatomic, strong) NSString* showHtmlString;


@end
