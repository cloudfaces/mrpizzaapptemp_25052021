//
//  CFContentView.m
//  app
//
//  Created by LeyLa on 3/8/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFContentView.h"
#import "CFPage+JSON.h"
#import "UIColor+Additions.h"
#import "UIImage+Asset.h"

@implementation CFContentView


#pragma mark BSJsonSupported

- (id)proxyForJson
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    if (self.type) {
        [dict setObject:self.type forKey:@"type"];
    }
    if (self.color) {
        [dict setObject:[self.color hexString] forKey:@"color"];
    }
    if (self.text) {
        [dict setObject:self.text forKey:@"text"];
    }
    return dict;
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    if ([super init]) {
        if (!receivedObjects) {
            return nil;
        }
        receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];
        self.type = receivedObjects[@"type"];
        self.color = [UIColor colorFromHexString:receivedObjects[@"color"]];
        self.text = receivedObjects[@"text"];
    }
    return self;
}

@end
