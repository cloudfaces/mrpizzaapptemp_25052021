//
//  LocationShareModel.m
//  app
//
//  Created by LeyLa on 8/23/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "LocationShareModel.h"
static LocationShareModel *sharedInstance = nil;

@implementation LocationShareModel
+ (id)sharedModel
{
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}



@end
