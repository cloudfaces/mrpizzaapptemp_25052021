//
//  LocationRetriever.m
//  app
//
//  Created by Bernhard Kunnert on 07.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BackgroundLocationRetriever.h"
#import "LocationCoder.h"
#import "CFHttpRequest.h"


@interface BackgroundLocationRetriever () <CLLocationManagerDelegate>


@property(nonatomic, strong) CLLocationManager* locationManager;
@property(nonatomic, strong) NSMutableArray* locationQueue;
@property(nonatomic, copy) NSString* url;


@end
@implementation BackgroundLocationRetriever

+ (instancetype) instance {
    static BackgroundLocationRetriever* retriever = nil;
    if (!retriever) {
        retriever = [[BackgroundLocationRetriever alloc] initInternal];
    }
    return retriever;
}

- (instancetype) init {
    [NSException raise:@"Invalid" format:@"use instance to create instance of this type"];
    return nil;
}

- (instancetype) initInternal {
    self = [super init];
    if (self) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationQueue = [[NSMutableArray alloc] init];
        
        _isActive = @"YES";
    }
    return self;
}

- (BOOL) isAuthorized:(CLAuthorizationStatus) status {
    return status == kCLAuthorizationStatusAuthorizedAlways;
}

- (void) startLocationUpdates:(NSString*) url
{
    self.url = url;
    
    if (![self isAuthorized:[CLLocationManager authorizationStatus]]) {
        [self.locationManager requestAlwaysAuthorization];
    } else {
        [self.locationManager startUpdatingLocation];
    }
}

- (void) sendLocationToServer:(CLLocation*) location
{
    CFHttpRequest* request = [[CFHttpRequest alloc] init];
    request.url = self.url;
    request.method = @"POST";
    request.body = [LocationCoder stringFromLocation:location];
    
    [request send];
}

- (NSArray*) getQueuedLocations
{
    NSArray* locations = [self.locationQueue copy];
    [self.locationQueue removeAllObjects];
    return locations;
}

- (void) stopLocationUpdates {
    [self.locationManager stopUpdatingLocation];
    [self.locationQueue removeAllObjects];
}

#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    
    [self.locationQueue addObjectsFromArray:locations];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_UPDATE object:self.locationManager.location];
    if (self.url) {
        [self sendLocationToServer:self.locationManager.location];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if ([self isAuthorized:[CLLocationManager authorizationStatus]]) {
        [self.locationManager startUpdatingLocation];
    }
}

@end
