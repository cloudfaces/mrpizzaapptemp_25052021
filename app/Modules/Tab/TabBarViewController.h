//
//  TabBarViewController.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Navigation.h"

@interface TabBarViewController : BaseTabViewController <Navigation>

@end
