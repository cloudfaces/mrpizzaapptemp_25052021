//
//  DefaultNavigationImpl.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CFPage.h"
#import "Navigation.h"
#import "BadgeBarButtonItem.h"
#import "Badges.h"
#import "NavigationWebViewStack.h"
#import "SlidingViewController.h"

@class DefaultNavigationImpl;

@protocol DefaultNavigationImplResultDelegate <NSObject>

- (void) navigationCompleted:(DefaultNavigationImpl*) navigation withResult:(id) result;

@end

@interface DefaultNavigationImpl : NSObject <NavigationService>
@property NavigationWebViewStack *context;

@property (strong, nonatomic) UINavigationController* nav;

@property (readonly, nonatomic, weak) UIViewController<Navigation>* owner;
@property (readonly, nonatomic) id navigationResult;
@property (weak, nonatomic) id<DefaultNavigationImplResultDelegate> resultDelegate;

//rigtMenuView
//@property (strong, nonatomic) NSString *rightView;

@property (strong, nonatomic) NSString *menuPosition;


- (id) initWithOwner:(UIViewController<Navigation>*) owner;

- (void) navigateToPage:(CFPage *)page withContext:(id) navigationContext;
- (void) navigateToPageAndAddToStack:(CFPage *)page withContext:(id) navigationContext;

- (void) navigateBackWithResult:(id) result;
- (void) navigateBackWithResult:(id)result onStackView:(NSString *) pageId;
- (void) navigateBackWithResultToRoot:(id)result onStackView:(NSString *) pageId;

+ (void) applyPageHeaderAttributes:(CFPageHeader*) header toNavigationController:(UINavigationController*) navigationController andButtonTarget:(id<ButtonTarget>) target;
+ (void) clearPageHeaderAttributes:(UINavigationController*) navigationController;
- (void) delegateParentPageHeaderAttributes:(CFPageHeader*) header toNavigationController:(UINavigationController*) navigationController andButtonTarget:(id<ButtonTarget>) target;
+ (void) clearView:(UINavigationController*) navigationController;
+ (void) changeTitle:(UINavigationController*) navigationController newTitle: (NSString *) newTitle;
+ (void) updateBadgeNew:(UINavigationController*) navigationController;

+ (void) setMenuPosition:(NSString*) position;
- (UIViewController<Navigation>*) createAndParametrizeViewController:(CFPage*) page withContext:(id)navigationContext;


@end
