//
//  UIImage+Asset.h
//  app
//
//  Created by Bernhard Kunnert on 11.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Asset)

+ (UIImage*) imageFromAsset:(NSString*) imageName;
+ (UIImage*) navBarImageFromAsset:(NSString*) imageName;

@end
