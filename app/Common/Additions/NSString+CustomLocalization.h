//
//  NSString+NSString_Additions.h
//  mobile-pocket
//
//  Created by Simon Moser on 04.11.11.
//  Copyright (c) 2011 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CustomLocalization)
    + (NSString*) localize:(NSString*) key;
@end
