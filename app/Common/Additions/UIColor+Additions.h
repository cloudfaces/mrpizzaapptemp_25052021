//
//  UIColor+Additions.h
//  app
//
//  Created by Bernhard Kunnert on 06.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Additions)

+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (NSString *) hexStringFromColor:(UIColor *)color;
- (NSString *) hexString;
- (BOOL)isEqualToColor:(UIColor *)otherColor;

@end
