//
//  NSString+NSString_Additions.h
//  mobile-pocket
//
//  Created by Simon Moser on 04.11.11.
//  Copyright (c) 2011 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_Additions)
    - (NSString *)MD5;

/**
 *  Inserts a space character every columnWidth characters as long as the characters are comprised of numbers. 
 *  Any other text will remain untoched. Separattion is reset if any character other than a digit is placed between numbers.
 */
- (NSString*) numberStringInColumns:(NSInteger) columnWidth;
- (NSString*) stripHtml;
- (BOOL) isEmpty;

+ (BOOL) isEmpty:(NSString*) string;

@end
