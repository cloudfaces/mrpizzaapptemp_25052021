//
//  BaseTabViewController.m
//  app
//
//  Created by Bernhard Kunnert on 18.12.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BaseTabViewController.h"

@interface BaseTabViewController ()

@end

@implementation BaseTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)init {
	if (self = [super init]) {
		self.delegate = self;
        UIImage *tabbarBgImage = [UIImage imageNamed:@"tabbar_bg.png"];
        if(tabbarBgImage != nil) {
            CGRect frame = CGRectMake(0, 0, 480, 49);
            UIView *v = [[UIView alloc] initWithFrame:frame];
            UIColor *c = [[UIColor alloc] initWithPatternImage:tabbarBgImage];
            v.backgroundColor = c;
            [[self tabBar] addSubview:v];
        }
	}
	return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if(UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
		return YES;
	}
    return NO;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
	UILabel* titleLabel = (UILabel*)self.navigationItem.titleView ;
    
	self.navigationItem.rightBarButtonItem = viewController.navigationItem.rightBarButtonItem;
	self.navigationItem.backBarButtonItem = viewController.navigationItem.backBarButtonItem;
	self.title = viewController.title;
	titleLabel.text = self.title;
}


- (void) makeTabBarHidden:(BOOL)hide animated:(BOOL) animated{
	if ( [self.view.subviews count] < 2 ) {
		return;
	}
    
    if(animated) {
        [UIView beginAnimations:@"tabbarHide" context:nil];
        [UIView setAnimationDuration:0.4];
    }
    
	UIView *contentView;
    
	if ( [[self.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] ) {
		contentView = [self.view.subviews objectAtIndex:1];
	} else {
		contentView = [self.view.subviews objectAtIndex:0];
	}
    
	if (hide) {
		contentView.frame = self.view.bounds;
	}
	else {
		contentView.frame = CGRectMake(self.view.bounds.origin.x,
                                       self.view.bounds.origin.y,
                                       self.view.bounds.size.width,
                                       self.view.bounds.size.height - self.tabBar.frame.size.height);
	}
    
	self.tabBar.alpha = hide ? 0.0 : 1.0;
    
    if(animated) {
        [UIView commitAnimations];
    }
    self.tabBarHidden = hide;
}

@end
