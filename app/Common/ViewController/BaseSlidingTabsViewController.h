////
////  BaseSlidingTabsViewController.h
////  app
////
////  Created by LeyLa on 5/3/18.
////  Copyright © 2018 CloudFaces. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//
//@interface BaseSlidingTabsViewController : UITabBarController <UITabBarDelegate, UITabBarControllerDelegate>
//
//- (void) makeTabBarHidden:(BOOL)hide animated:(BOOL) animated;
//
//@property BOOL tabBarHidden;
//
//@end

