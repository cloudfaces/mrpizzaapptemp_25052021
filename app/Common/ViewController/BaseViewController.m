//
//  BaseViewController.m
//  app
//
//  Created by Simon Moser on 24.02.12.
//  Copyright (c) 2012 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BaseViewController.h"
#import "BundleSettings.h"
#import "MRProgress.h"

@implementation BaseViewController

@synthesize bs;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.bs = [BundleSettings instance];
        self.hidesBottomBarWhenPushed = YES;
    }
    
    return self;
}

-(void) viewDidLoad
{
    [super viewDidLoad];
#ifdef FRANK_CUCUMBER
    // speed up animations for ui testing to make tests faster
    self.view.layer.speed = 5;
#endif
}

- (void)startWaitingAnimation
{
    
    [MRProgressOverlayView showOverlayAddedTo:self.navigationController.view animated:YES];
}

- (void)stopWaitingAnimaiton
{
    [MRProgressOverlayView dismissOverlayForView:self.navigationController.view animated:YES];
}

@end
